/*
 * File:    ActivationFrame.java
 *
 * Course:  CSE222
 * Project: HW04
 * Created: 06.04.2013 14:46:12
 * 
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */
package com.furkantektas.assignments.cse222.gitl;

import java.util.ArrayList;

import com.furkantektas.assignments.cse222.gitl.GITExceptions.GITNonUniqueObjectException;
import com.furkantektas.assignments.cse222.gitl.GITExceptions.GITUninitializedVariableException;

/**
 * @author Furkan
 * @version 1.0
 * @created 06-04-2013 15:11:06
 */
public class ActivationFrame {

	/**
	 * Variable names and values. varValues can be Integer or Float.
	 */
	private ArrayList<Variable> variables = new ArrayList<Variable>();
	
	private Context context;
	
	/**
	 * First line index of the current activation frame(scope)
	 */
	private int firstLineIndex = 0;
	
	/**
	 * Scope mode. {@link Context#SCOPE_UNNAMED_MODE} for unnamed scopes.
	 * {@link Context#SCOPE_LOOPMODE} for loops.
	 * Default: {@link Context#SCOPE_UNNAMED_MODE}
	 */
	private char scopeMode = Context.SCOPE_UNNAMED_MODE;

	/**
	 * Loop counter variable
	 */
	private Variable loopCounterVariable;

	/**
	 * Loop condition variable
	 */
	private Variable loopCondition;
	
	


	public ActivationFrame(Context context, int firstLineIndex) {
		this.firstLineIndex = firstLineIndex;
		this.context = context;
	}
	
	public ActivationFrame(Context context, int firstLineIndex, char scopeMode) {
		this(context, firstLineIndex);
		this.scopeMode = scopeMode;
	}
	
	public ActivationFrame(Context context) {
		this(context, 0);
	}

	/**
	 * Returns the loop counter variable's value.
	 * @return the loop counter variable's value.
	 */
	public Integer getLoopCounterVariable() {
		return (Integer) loopCounterVariable.value;
	}
	
	/**
	 * Sets the loop counter variable
	 * @param loopCounterVariable loop counter variable
	 * @throws GITExceptions if scopeMode is not {@link Context#SCOPE_LOOPMODE}
	 */
	public void setLoopCounterVariable(Variable loopCounterVariable) throws GITExceptions {
		if(scopeMode != Context.SCOPE_LOOPMODE)
			throw new GITExceptions("Internal Error. #501");
		this.loopCounterVariable = loopCounterVariable;
		addVariable(loopCounterVariable);
	}
	
	/**
	 * Creates and sets the loop counter variable
	 * @param loopCounterVariable loop counter variable
	 * @throws GITExceptions if scopeMode is not {@link Context#SCOPE_LOOPMODE}
	 */
	public void setLoopCounterVariable(String varName, String varValue) throws GITExceptions {
		Variable v = new Variable(varName, Integer.valueOf(varValue));
		setLoopCounterVariable(v);
	}

	/**
	 * Returns the loop condition's value.
	 * @return the loop condition's value.
	 */
	public Integer getLoopCondition() {
		return (Integer) loopCondition.value;
	}
	
	/**
	 * Sets the loop condition's value.
	 * @param loopCondition
	 * @throws GITExceptions if scopeMode is not {@link Context#SCOPE_LOOPMODE}
	 */
	public void setLoopCondition(Variable loopCondition) throws GITExceptions {
		if(scopeMode != Context.SCOPE_LOOPMODE)
			throw new GITExceptions("Internal Error. #501");
		this.loopCondition = loopCondition;
		addVariable(loopCondition);
	}
	
	/**
	 * Sets the loop condition's value with name "__LoopCondition"
	 * @param loopCondition
	 * @throws GITExceptions if scopeMode is not {@link Context#SCOPE_LOOPMODE}
	 */
	public void setLoopCondition(String value) throws GITExceptions {
		if(scopeMode != Context.SCOPE_LOOPMODE)
			throw new GITExceptions("Internal Error. #501");
		Variable v = context.getVariable(value);

		if(v == null)
			v = new Variable("__LoopCondition" , Integer.valueOf(value));
		setLoopCondition(v);
	}

	public Number getValue(String varName) throws GITUninitializedVariableException {
		Variable v = getVariable(varName);
		if (v != null) {
			if(v.value != null)
				return v.value;
			else 
				throw new GITExceptions.GITUninitializedVariableException
				("Uninitialized variable: "+v.name);
		}
		return null;
	}
	
	/**
	 * Increases loopCounterVariable and checks the current state of loop.
	 * If loop counter is not smaller than loop condition returns true, unless false.
	 * @return loopCounter < loopCondition
	 * @throws GITExceptions 
	 */
	public boolean loopEnd() throws GITExceptions {
		if(scopeMode != Context.SCOPE_LOOPMODE)
			throw new GITExceptions("Internal Error. #501");
		loopCounterVariable.value = ((Integer) loopCounterVariable.value) + 1;
		if(getLoopCounterVariable() <= getLoopCondition())
			return false;
		return true;
	}
	
	/**
	 * Adds a new variable to the variables array. If check is true, checks
	 * variables array before attempting adding a new variable to avoid
	 * conflicting variable names. If there is a variable with name varName and
	 * check is true, throws {@link GITNonUniqueObjectException}. If check is false, adds a
	 * new Variable to variables without checking there is another variable with
	 * the same name.
	 * 
	 * @param v - Variable
	 * @param check - Check variables before add.
	 * @return Variable value.
	 * @throws GITNonUniqueObjectException
	 */
	public Number addVariable(Variable v, boolean check)
			throws GITNonUniqueObjectException {
		if (check && getVariable(v.name) != null)
			throw new GITNonUniqueObjectException("Cannot add variable." + v.name
					+ " is already used for another variable");
		variables.add(v);
		return v.value;
	}
	
	/**
	 * Adds a new variable to the variables array. If check is true, checks
	 * variables array before attempting adding a new variable to avoid
	 * conflicting variable names. If there is a variable with name varName and
	 * check is true, throws {@link GITNonUniqueObjectException}. If check is false, adds a
	 * new Variable to variables without checking there is another variable with
	 * the same name.
	 * 
	 * @param varName
	 *            - Variable Name
	 * @param varValue
	 *            - Variable Value
	 * @param check
	 *            - Check variables before add.
	 * @return Variable value.
	 * @throws GITNonUniqueObjectException
	 */
	public Number addVariable(String varName, Number varValue, boolean check)
			throws GITNonUniqueObjectException {
		return addVariable(new Variable(varName, varValue));
	}

	/**
	 * Adds a new variable to the variables array. Checks variables array before
	 * attempting adding a new variable to avoid conflicting variable names. If
	 * there is a variable with name varName, throws {@link GITNonUniqueObjectException}.
	 * 
	 * @param varName
	 *            - Variable Name
	 * @param varValue
	 *            - Variable Value
	 * @return Variable value.
	 * @throws GITNonUniqueObjectException
	 */
	public Number addVariable(String varName, Number varValue)
			throws GITNonUniqueObjectException {
		return addVariable(varName, varValue, true);
	}
	
	/**
	 * Adds a new variable to the variables array. Checks variables array before
	 * attempting adding a new variable to avoid conflicting variable names. If
	 * there is a variable with name varName, throws {@link GITNonUniqueObjectException}.
	 * 
	 * @param v - Variable 
	 * @return Variable value.
	 * @throws GITNonUniqueObjectException
	 */
	public Number addVariable(Variable v) 
			throws GITNonUniqueObjectException {
		addVariable(v, true);
		return v.value;
	}

	/**
	 * Sets the value of variable named with varName value to varValue. If there
	 * is no variable with varName, adds a new variable with varName and
	 * varValue.
	 * 
	 * @param varName
	 * @param varValue
	 * @return
	 */
	public Number setValue(String varName, Number varValue) {
		Variable v = getVariable(varName);
		if (v != null) {
			v.value = varValue;
			return varValue;
		}
		/* no variable found. */
		try {
			return addVariable(varName, varValue, false);
		} catch (GITNonUniqueObjectException e) {
			/* Dead code :) */
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Returns the Variable instance with given variable name. If there is no
	 * such variable, returns null.
	 * 
	 * @param varName
	 *            - Name of Variable object to be returned
	 * @return name of Variable object or null if there is no variable with
	 *         given name.
	 */
	Variable getVariable(String varName) {
		for (Variable v : variables)
			if (v.name.equals(varName))
				return v;
		return null;
	}

	/**
	 * Variable holder.
	 * name is the name of the variable
	 * value is either Integer or Float.
	 * @author Furkan
	 *
	 */
	static class Variable {
		private String name;
		private Number value;

		Variable(String name, Number value) {
			this.name = name;
			this.value = value;
		}
	}
	
	/**
	 * Returns the index of the scope's first line.
	 * @return index of the scope's first line.
	 */
	public int getFirstLineIndex() {
		return firstLineIndex;
	}

	/**
	 * Returns the index of the scope's first line.
	 * @param firstLineIndex
	 */
	public void setFirstLineIndex(int firstLineIndex) {
		this.firstLineIndex = firstLineIndex;
	}
	
}// end ActivationFrame