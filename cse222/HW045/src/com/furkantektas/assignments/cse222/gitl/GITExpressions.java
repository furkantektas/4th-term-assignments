/*
 * File:    GITExpressions.java
 *
 * Course:  CSE222
 * Project: HW04
 * Created: 06.04.2013 14:46:12
 * 
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */
package com.furkantektas.assignments.cse222.gitl;

import java.util.Stack;

import com.furkantektas.assignments.cse222.gitl.GITExceptions.GITInvalidOperatorException;
import com.furkantektas.assignments.cse222.gitl.GITExceptions.GITSyntaxErrorException;
import com.furkantektas.assignments.cse222.gitl.GITExceptions.GITUninitializedVariableException;

/**
 * @author Furkan
 * @version 1.0
 * @created 07-04-2013 15:26:06
 */
public class GITExpressions {

	public static class GITFunctions extends GITExpressions {
		private static final String[] FUNCTIONS = { "print" };

		/**
		 * Returns true if FUNCTIONS array contains string function.
		 * 
		 * @param function
		 * @return true if function parameter is in FUNCTIONS array
		 */
		public static boolean isFunction(String function) {
			String lowerFunction = function.toLowerCase();
			for (String fn : FUNCTIONS)
				if (fn.equals(lowerFunction))
					return true;
			return false;
		}

		/**
		 * Evaluates the function with given rVal. Returns the functions'
		 * default return value.
		 * 
		 * @param rVal
		 * @return default functions' default value
		 */
		public static Number eval(String function, Number rVal) {
			if (!isFunction(function))
				return null;

			/* print */
			if (function.equals(FUNCTIONS[0])) {
				System.out.println(rVal);
				return rVal;
			}
			return null;
		}

	}

	public static class GITVariables extends GITExpressions {
		private static final String[] VARIABLES = { "int", "float" };

		/**
		 * Returns true if VARIABLES array contains string variable.
		 * 
		 * @param variable
		 * @return true if variable parameter is in VARIABLES array
		 */
		public static boolean isVariableDeclaration(String variable) {
			for (String var : VARIABLES)
				if (var.equals(variable))
					return true;
			return false;
		}
	}

	public static class GITPostFixEvaluator extends GITExpressions {

		private Context context;
		private Stack<Number> stack = new Stack<Number>();

		public GITPostFixEvaluator(Context context) {
			this.context = context;
		}

		/**
		 * Checks if the op is an operator.
		 * @param op
		 * @return true if op is a valid operator
		 */
		public boolean isOPerator(char op) {
			return InfixToPostfixConverter.isOperator(op);
		}

		/**
		 * Evaluates the postfix string.
		 * 
		 * @param postfix
		 * @return the number evaluated.
		 * @throws GITSyntaxErrorException
		 *             - Syntax error problem.
		 * @throws GITUninitializedVariableException
		 *             - When there is a uninitialized variables usage
		 * @throws GITInvalidOperatorException
		 *             - When there is an invalid operator
		 */
		public Number eval(String postfix) throws GITSyntaxErrorException,
				GITUninitializedVariableException, GITInvalidOperatorException {
			String[] tokens = postfix.split("\\s");

			for (int i = 0; i < tokens.length; ++i) {

				if (isOPerator(tokens[i].charAt(0))) {
					/* Operator found */

					if (tokens[i].length() != 1)
						throw new GITInvalidOperatorException(
								"Invalid Operator: " + tokens[i]);

					Number operand2 = (Number) stack.pop();
					Number operand1 = (Number) stack.pop();
					char operator = tokens[i].charAt(0);

					if (operand1 instanceof Integer
							&& operand2 instanceof Integer) {
						Integer op1 = (Integer) operand1;
						Integer op2 = (Integer) operand2;
						stack.push(evalBinaryOperator(op1, op2, operator));
					} else if (operand1 instanceof Float
							&& operand2 instanceof Float) {
						Float op1 = (Float) operand1;
						Float op2 = (Float) operand2;
						stack.push(evalBinaryOperator(op1, op2, operator));
					} else
						throw new GITExceptions.GITSyntaxErrorException(
								"Mixed types error.");
				} else if (Interpreter.isNumeric(tokens[i])) {
					stack.push(getNumber(tokens[i]));
				} else {
					Number val = null;
					if ((val = context.getVariableValue(tokens[i])) == null)
						throw new GITExceptions.GITInvalidOperatorException(
								"Invalid Operator: " + tokens[i]);
					stack.push(val);
				}
			}

			return stack.pop();
		}

		/**
		 * Evaluates binary operations for Integers.
		 * @param op1
		 * @param op2
		 * @param operator
		 * @return
		 */
		private Integer evalBinaryOperator(Integer op1, Integer op2,
				char operator) {
			switch (operator) {
			case '+':
				return (op1 + op2);
			case '-':
				return (op1 - op2);
			case '*':
				return (op1 * op2);
			case '/':
				return (op1 / op2);
			case '^':
				return Integer.valueOf((int) Math.pow(op1, op2));
			default:
				return 0;
			}
		}

		/**
		 * Evalueates binary operations for Floats.
		 * @param op1
		 * @param op2
		 * @param operator
		 * @return
		 */
		private Float evalBinaryOperator(Float op1, Float op2, char operator) {
			switch (operator) {
			case '+':
				return (op1 + op2);
			case '-':
				return (op1 - op2);
			case '*':
				return (op1 * op2);
			case '/':
				return (op1 / op2);
			case '^':
				return Float.valueOf((float) Math.pow(op1, op2));
			default:
				return 0F;
			}
		}

		/**
		 * Parses string value to the either Integer or Float object.
		 * @param val
		 * @return Parsed Number reference of val.
		 */
		public static Number getNumber(String val) {
			if (val.contains("."))
				return Float.valueOf(val);
			return Integer.valueOf(val);

		}
	}

}// end Expressions