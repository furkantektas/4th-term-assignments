package com.furkantektas.assignments.cse222.gitl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.furkantektas.assignments.cse222.gitl.GITExceptions.GITInvalidFileNameException;

public class GITCodeImporter {

	/**
	 * Reads and puts the file content to the returned List<String>.
	 * @param path - Input program file path. 
	 * @return List<String>. Each element of the list
	 * holds a line of the program.
	 * @throws GITExceptions.GITInvalidFileNameException - 
	 * File extension should be .git 
	 * @throws IOException - If file could not be opened.
	 */
	public static List<String> importCode(String path) 
			throws GITInvalidFileNameException, IOException {
		if (!checkFileName(path))
			throw new GITExceptions.GITInvalidFileNameException(
					"Invalid file name.");
		ArrayList<String> program = new ArrayList<String>();

		BufferedReader fileBuffer = new BufferedReader(new FileReader(path));
		String line = null;
		
		while ((line = fileBuffer.readLine()) != null)
			program.add(line);

		fileBuffer.close();
		
		return program;
	}
	
	public static boolean checkFileName(String path) {
		int i = path.lastIndexOf('.');
		if (i > 0)
			return "git".equals(path.substring(i+1));
		
		return false;
	}

}
