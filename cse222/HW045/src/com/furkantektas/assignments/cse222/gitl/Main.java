/*
 * File:    Main.java
 *
 * Course:  CSE222
 * Project: HW04
 * Created: 06.04.2013 14:46:12
 * 
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */
package com.furkantektas.assignments.cse222.gitl;

import java.util.List;

/**
 * @author Furkan
 * @version 1.0
 * @created 06-04-2013 15:58:49
 */
public class Main {

	/**
	 * Main function
	 * 
	 * @param args
	 *            args
	 */
	public static void main(String... args) {

		try {
			// Reading file
			List<String> program = GITCodeImporter.importCode(args[0]);

			// Creating the base context
			Context context = new Context(program);
			
			// Interpreting the program
			Interpreter intrp = new Interpreter(context);
			intrp.interpret();

		} catch (Exception e) {
			// Invalid File
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}

}// end Main