/*
 * File:    Interpreter.java
 *
 * Course:  CSE222
 * Project: HW04
 * Created: 06.04.2013 14:46:12
 * 
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */
package com.furkantektas.assignments.cse222.gitl;

import java.util.Arrays;

import com.furkantektas.assignments.cse222.gitl.GITExceptions.GITEmptyStackException;
import com.furkantektas.assignments.cse222.gitl.GITExceptions.GITInvalidOperatorException;
import com.furkantektas.assignments.cse222.gitl.GITExceptions.GITInvalidVariableException;
import com.furkantektas.assignments.cse222.gitl.GITExceptions.GITNonUniqueObjectException;
import com.furkantektas.assignments.cse222.gitl.GITExceptions.GITSyntaxErrorException;
import com.furkantektas.assignments.cse222.gitl.GITExceptions.GITUninitializedVariableException;
import com.furkantektas.assignments.cse222.gitl.GITExpressions.GITPostFixEvaluator;

/**
 * @author Furkan
 * @version 1.0
 * @created 06-04-2013 17:59:19
 */
public class Interpreter {

	private Context context;

	public Interpreter(Context context) {
		this.context = context;
	}

	/**
	 * Interprets the program by calling interpretLine().
	 * 
	 * @throws GITExceptions
	 * @throws GITInvalidVariableException
	 *             - If there is no variable with given name
	 * @throws GITNonUniqueObjectException
	 *             - for redefinition of variable within the same context
	 * @throws GITSyntaxErrorException
	 *             - Syntax error problem.
	 * @throws GITEmptyStackException
	 * @throws GITUninitializedVariableException
	 * @throws GITInvalidOperatorException
	 */
	public void interpret() throws GITExceptions {
		context.setCurrentLineNumber(0);
		while (context.hasMoreLines())
			interpretLine();
	}

	/**
	 * Interprets a line and updates the context's current line number by 1.
	 * 
	 * @throws GITExceptions
	 * @throws GITInvalidVariableException
	 *             - If there is no variable with given name
	 * @throws GITNonUniqueObjectException
	 *             - for redefinition of variable within the same context
	 * @throws GITSyntaxErrorException
	 *             - Syntax error problem.
	 * @throws GITEmptyStackException
	 * @throws GITUninitializedVariableException
	 * @throws GITInvalidOperatorException
	 */
	private void interpretLine() throws GITExceptions {

		String expression = context.getCurrentLine();
		interpretExpression(expression);
		context.setCurrentLineNumber(context.getCurrentLineNumber() + 1);
	}

	/**
	 * Interprets the given extension. This method is the core of interpreting
	 * mechanism. Any expression can be calculated.
	 * 
	 * @param tokens
	 *            - Separated expression.
	 * @return Calculated return value.
	 * @throws GITExceptions
	 * @throws GITInvalidVariableException
	 *             - If there is no variable with given name
	 * @throws GITNonUniqueObjectException
	 *             - for redefinition of variable within the same context
	 * @throws GITSyntaxErrorException
	 *             - Syntax error problem.
	 * @throws GITEmptyStackException
	 * @throws GITUninitializedVariableException
	 * @throws GITInvalidOperatorException
	 */
	private Number interpretExpression(String[] tokens) throws GITExceptions {

		if (tokens.length < 1)
			throw new GITExceptions.GITSyntaxErrorException("Invalid line: "
					+ context.getCurrentLineNumber());
		else if (tokens.length <= 2) {
			// Will be used for variables
			Number var;

			if (isNumeric(tokens[0]))
				return GITExpressions.GITPostFixEvaluator.getNumber(tokens[0]);
			else if ((var = context.getVariableValue(tokens[0])) != null)
				return var;
			else if (GITExpressions.GITVariables
					.isVariableDeclaration(tokens[0])) {
				context.addVariable(tokens[0], tokens[1]);
			} else if (isScopeBegin(tokens)) {
				startScope(tokens, Context.SCOPE_UNNAMED_MODE);
				return 0;
			} else if (isScopeEnd(tokens)) {
				if (context.loopEnd())
					return 0;
				// context.setCurrentLineNumber(context.getCurrentLineNumber());
				else
					context.setCurrentLineNumber(context
							.getScopesFirstLineIndex());
				return 0;

			} else if (isValidLoopStatement(tokens)) {
				startLoop(tokens);

				if (isScopeBegin(splitAndTrim(context.getLine(context
						.getCurrentLineNumber() + 1))))
					context.setCurrentLineNumber(context.getCurrentLineNumber() + 1);

			} else if (GITExpressions.GITFunctions.isFunction(tokens[0])) {
				// First token is a function
				String[] newTokens = new String[tokens.length - 1];
				newTokens = Arrays.copyOfRange(tokens, 1, tokens.length);
				Number rVal = interpretExpression(newTokens);

				return GITExpressions.GITFunctions.eval(tokens[0], rVal);
			} else
				throw new GITInvalidVariableException("Invalid variable: '"
						+ tokens[0] + "' at line: "
						+ (context.getCurrentLineNumber() + 1));

		} else {
			if (GITExpressions.GITVariables.isVariableDeclaration(tokens[0])) {
				// First token is a variable declaration
				if (tokens.length == 3 && isNumeric(tokens[2]))
					context.addVariable(tokens[0], tokens[1], tokens[2]);
				else
					throw new GITSyntaxErrorException("Invalid initial value: "
							+ tokens[2] + "' at line: "
							+ (context.getCurrentLineNumber() + 1));
			} else if (tokens[1].equals("=")) {
				StringBuilder infix = new StringBuilder();

				for (int i = 2; i < tokens.length; ++i)
					infix.append(tokens[i]).append(" ");

				InfixToPostfixConverter i2p = new InfixToPostfixConverter();
				String postfixExp = i2p.convert(infix.toString());
				GITPostFixEvaluator gpe = new GITPostFixEvaluator(context);

				Number rVal = gpe.eval(postfixExp);
				if (!context.setValue(tokens[0], rVal))
					throw new GITExceptions.GITSyntaxErrorException(
							"Invalid assignment or variable." + "' at line: "
									+ (context.getCurrentLineNumber() + 1));
				return rVal;
			} else {
				InfixToPostfixConverter i2p = new InfixToPostfixConverter();
				String postfixExp = i2p.convert(tokens);
				GITPostFixEvaluator gpe = new GITPostFixEvaluator(context);

				return gpe.eval(postfixExp);
			}
		}
		return 0;
	}

	/**
	 * Returns true if tokens[0] is "begin"
	 * 
	 * @param tokens
	 * @return true for tokens[0] == "begin"
	 */
	private boolean isScopeBegin(String[] tokens) {
		return ("begin".equals(tokens[0]));
	}

	/**
	 * Returns true if tokens[0] is "end"
	 * 
	 * @param tokens
	 * @return tokens[0] == "end"
	 */
	private boolean isScopeEnd(String[] tokens) {
		return ("end".equals(tokens[0]));
	}

	/**
	 * Returns true if tokens are valid tokens for initializing a loop
	 * 
	 * @param tokens
	 *            [0] => "loop" [1]=> conditions separated with ':'
	 * @return true for valid loop initialization
	 * @throws GITSyntaxErrorException
	 *             if tokens are not valid.
	 */
	private boolean isValidLoopStatement(String[] tokens)
			throws GITSyntaxErrorException {
		if ("loop".equals(tokens[0])) {
			if (tokens[1].split(":").length != 3)
				throw new GITSyntaxErrorException(
						"Invalid loop initialization.");
			return true;
		}
		return false;
	}

	/**
	 * Creates a new activation frame for current scope, creates and initializes
	 * the loop condition variables.
	 * 
	 * @param tokens
	 * @param scopeMode
	 *            {@link Context#SCOPE_LOOPMODE} for loops.
	 *            {@link Context#SCOPE_UNNAMED_MODE} for unnamed scopes.
	 */
	private void startScope(String[] tokens, char scopeMode) {
		context.addActivationFrame();
	}

	private void startLoop(String[] tokens) throws GITExceptions {
		context.addActivationFrame(Context.SCOPE_LOOPMODE);
		String[] conditions = splitAndTrim(tokens[1], ":");
		try {
			context.setLoopCounterVariable(conditions[0], conditions[1]);
			context.setLoopCondition(conditions[2]);
		} catch (GITInvalidVariableException e) {
			// should not work
			e.printStackTrace();
		} catch (GITNonUniqueObjectException e) {
			// should not work
			e.printStackTrace();
		}
	}

	/**
	 * Splits the expression to the parts and call interpretExpression(String[])
	 * 
	 * @param expression
	 *            - Expression separated by white space chars
	 * @return Calculated return value.
	 * @throws GITExceptions
	 * @throws GITInvalidVariableException
	 *             - If there is no variable with given name
	 * @throws GITNonUniqueObjectException
	 *             - for redefinition of variable within the same context
	 * @throws GITSyntaxErrorException
	 *             - Syntax error problem.
	 * @throws GITEmptyStackException
	 * @throws GITUninitializedVariableException
	 * @throws GITInvalidOperatorException
	 */
	private Number interpretExpression(String expression) throws GITExceptions {
		String[] tokens = splitAndTrim(expression);
		return interpretExpression(tokens);
	}

	/**
	 * Regular Expression based check fot if str is numeric.
	 * 
	 * @param str
	 * @return true if the str is a number
	 */
	public static boolean isNumeric(String str) {
		return str.matches("[+-]?\\d*(\\.\\d+)?");
	}

	public static String[] splitAndTrim(String expression) {
		return expression.split("\\s+");
	}

	public static String[] splitAndTrim(String expression, String separator) {
		String[] tokens = expression.split(separator);
		for (String token : tokens)
			token = token.trim();
		return tokens;
	}

}// end Interpreter