/*
 * File:    GITExceptions.java
 *
 * Course:  CSE222
 * Project: HW04
 * Created: 06.04.2013 14:46:12
 * 
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */
package com.furkantektas.assignments.cse222.gitl;

public class GITExceptions extends Exception {
	private static final long serialVersionUID = 6139005650094868250L;

	public GITExceptions(String msg) {
		super(msg);
	}
	
	/* Exception Classes */
	public static class GITSyntaxErrorException extends GITExceptions{
		private static final long serialVersionUID = -713262886759885379L;

		public GITSyntaxErrorException(String msg) {
			super(msg);
		}
	}
	
	public static class GITEmptyStackException extends GITExceptions{
		private static final long serialVersionUID = 5055765842682418416L;

		public GITEmptyStackException(String msg) {
			super(msg);
		}
	}
	
	public static class GITInvalidVariableException extends GITExceptions{
		private static final long serialVersionUID = 414836772371359244L;

		public GITInvalidVariableException(String msg) {
			super(msg);
		}
	}
	
	public static class GITNonUniqueObjectException extends GITExceptions{
		private static final long serialVersionUID = -5957027470257614937L;

		public GITNonUniqueObjectException(String msg) {
			super(msg);
		}
	}
	
	public static class GITUninitializedVariableException extends GITExceptions{
		private static final long serialVersionUID = 5361331194079156759L;

		public GITUninitializedVariableException(String msg) {
			super(msg);
		}
	}
	
	public static class GITInvalidOperatorException extends GITExceptions{
		private static final long serialVersionUID = -6811882678071987987L;

		public GITInvalidOperatorException(String msg) {
			super(msg);
		}
	}
	
	public static class GITInvalidFileNameException extends GITExceptions{
		private static final long serialVersionUID = -2505980902193219928L;

		public GITInvalidFileNameException(String msg) {
			super(msg);
		}
	}
}
