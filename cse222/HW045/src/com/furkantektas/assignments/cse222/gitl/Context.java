/*
 * File:    Context.java
 *
 * Course:  CSE222
 * Project: HW04
 * Created: 06.04.2013 14:46:12
 * 
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */
package com.furkantektas.assignments.cse222.gitl;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Stack;

import com.furkantektas.assignments.cse222.gitl.ActivationFrame.Variable;
import com.furkantektas.assignments.cse222.gitl.GITExceptions.GITInvalidVariableException;
import com.furkantektas.assignments.cse222.gitl.GITExceptions.GITNonUniqueObjectException;
import com.furkantektas.assignments.cse222.gitl.GITExceptions.GITUninitializedVariableException;
import com.furkantektas.assignments.cse222.gitl.GITExpressions.GITVariables;

/**
 * @author Furkan Tektas
 * @version 1.0
 * @created 06-04-2013 14:58:21
 */
public class Context {

	private int currentLineNumber;
	
	private ActivationFrame activeFrame;
	private Stack<ActivationFrame> activationFrames = new Stack<ActivationFrame>();
	private List<String> program;
	
	public static final char SCOPE_LOOPMODE = 'l';
	public static final char SCOPE_UNNAMED_MODE = 'u';
	
	public Context(List<String> program){
		this.program = program;
		activationFrames.add(new ActivationFrame(this));
		activeFrame = activationFrames.peek();
	}

	public int getCurrentLineNumber() {
		return currentLineNumber;
	}

	public String getCurrentLine() {
		return getLine(currentLineNumber);
	}
	
	public String getLine(int lineIndex) {
		return program.get(lineIndex);
	}
	
	public boolean hasMoreLines() {
		return (currentLineNumber < program.size());
	}
	
	public void setCurrentLineNumber(int currentLine) {
		this.currentLineNumber = currentLine;
	}

	/**
	 * Creates a new variable in the active activation stack with default 
	 * value 0.
	 * 
	 * @param type - Types defined in {@link GITVariables}
	 * @param name - Variable name
	 * @throws GITInvalidVariableException if type is not defined in
	 * {@link GITVariables}
	 * @throws GITNonUniqueObjectException if active activation stack 
	 * already contains a variable with name argument name.
	 */
	public void addVariable(String type, String name) 
			throws GITInvalidVariableException, GITNonUniqueObjectException {
		addVariable(type, name, null);
	}

	/**
	 * Creates a new variable in the active activation stack with value of
	 * value argument. 
	 * 
	 * @param type - Types defined in {@link GITVariables}
	 * @param name - Variable name
	 * @param value - Variable's default value
	 * @throws GITInvalidVariableException if type is not defined in
	 * {@link GITVariables}
	 * @throws GITNonUniqueObjectException if active activation stack 
	 * already contains a variable with name argument name.
	 */
	public void addVariable(String type, String name, String value) 
			throws GITInvalidVariableException, GITNonUniqueObjectException {
		
		Number o;
		if(value == null)
			o = null;
		else if(type.equals("int"))
			o = new Integer(Integer.valueOf(value));
		else if(type.equals("float"))
			o = new Float(Float.valueOf(value));
		else
			throw new GITExceptions.GITInvalidVariableException(
					"Invalid identifier: "+type);
		
		/* Creating a variable in the active activation frame */
		activeFrame.addVariable(name, o);
	}
	
	/**
	 * Returns the variable named with name. Method will search activation
	 * frames from the last, so that git has shadowing. 
	 * @param name - Variable name
	 * @return
	 * @throws GITUninitializedVariableException 
	 */
	public Number getVariableValue(String name) throws GITUninitializedVariableException {
		ListIterator<ActivationFrame> itr = activationFrames.listIterator(activationFrames.size());
		ActivationFrame af = null;
		
		Number value;
		
		while(itr.hasPrevious()) {
			af = itr.previous();
			if((value = af.getValue(name)) != null)
				return value;
		}
		
		return null;
	}
	
	/**
	 * Returns the variable instance with named "name". 
	 * If there is no such variable, returns null. 
	 * @param name
	 * @return
	 */
	Variable getVariable(String name) {
		ListIterator<ActivationFrame> itr = activationFrames.listIterator(activationFrames.size());
		ActivationFrame af = null;
		
		Variable variable;
		
		while(itr.hasPrevious()) {
			af = itr.previous();
			if((variable = af.getVariable(name)) != null)
				return variable;
		}
		
		return null;
	}

	
	/**
	 * Sets the value for variable with named variableName.
	 * Returns true if variable's value is set, false if there is
	 * no variable in activationframe stack.
	 * @param variableName - Variable Name
	 * @param value - New variable value
	 * @return true if variable's value is set, false if there is no
	 * such variable.
	 */
	public boolean setValue(String variableName, Number value) {
		Iterator<ActivationFrame> itr = activationFrames.iterator();
		ActivationFrame af = null;
				
		while(itr.hasNext()) {
			af = itr.next();
			if(af.setValue(variableName, value) != null)
				return true;
		}
		return false;
	}
	
	/**
	 * Adds an activation frame to activationFrames stack
	 * and sets the activeFrame to new frame.
	 * @param scopeMode {@link Context#SCOPE_UNNAMED_MODE} for unnamed scopes.
	 * {@link Context#SCOPE_LOOPMODE} for loops.
	 */
	public void addActivationFrame(char scopeMode) {
		activeFrame = new ActivationFrame(this,currentLineNumber+1, scopeMode);
		activationFrames.push(activeFrame);
	}
	
	/**
	 * Adds an activation frame to activationFrames stack
	 * and sets the activeFrame to new frame. 
	 * Scope mode is 'u'.
	 */
	public void addActivationFrame() {
		addActivationFrame(SCOPE_UNNAMED_MODE);
	}
	
	
	/**
	 * Returns the loop counter variable's value.
	 * @return the loop counter variable's value.
	 */
	public Integer getLoopCounterVariable() {
		return activeFrame.getLoopCounterVariable();
	}

	/**
	 * Creates and sets the loop counter variable
	 * @param loopCounterVariable loop counter variable
	 * @throws GITExceptions if scopeMode is not 'l'
	 */
	public void setLoopCounterVariable(String varName, String varValue) throws GITExceptions {
		activeFrame.setLoopCounterVariable(varName, varValue);
	}

	/**
	 * Returns the loop condition's value.
	 * @return the loop condition's value.
	 */
	public Integer getLoopCondition() {
		return activeFrame.getLoopCondition();
	}
	
	/**
	 * Sets the loop condition's value with name "__LoopCondition"
	 * @param loopCondition
	 * @throws GITExceptions if scopeMode is not {@link Context#SCOPE_LOOPMODE}
	 */
	public void setLoopCondition(String Variable) throws GITExceptions {
		activeFrame.setLoopCondition(Variable);
	}
	
	/**
	 * Increases loopCounterVariable and checks the current state of loop.
	 * If loop counter is not smaller than loop condition returns true, unless false.
	 * @return loopCounter < loopCondition
	 * @throws GITExceptions 
	 */
	public boolean loopEnd() throws GITExceptions {
		boolean result = activeFrame.loopEnd();
		if(result) {
			activationFrames.pop();
			activeFrame = activationFrames.peek();
		}
		return result;
	}
	
	/**
	 * Returns the index of the scope's first line.
	 * @return the index of the scope's first line.
	 */
	public int getScopesFirstLineIndex() {
		return activeFrame.getFirstLineIndex();
	}
	
}//end Context