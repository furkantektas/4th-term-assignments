/*
 * File:    InfixToPostfixConverter.java
 *
 * Course:  CSE222
 * Project: HW04
 * Created: 06.04.2013 14:46:12
 * 
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */
package com.furkantektas.assignments.cse222.gitl;

import java.util.*;

import com.furkantektas.assignments.cse222.gitl.GITExceptions.GITEmptyStackException;
import com.furkantektas.assignments.cse222.gitl.GITExceptions.GITInvalidOperatorException;
import com.furkantektas.assignments.cse222.gitl.GITExceptions.GITSyntaxErrorException;

/** Translates an infix expression with parentheses
*   to a postfix expression.
*   @author Koffman & Wolfgang
*/

public class InfixToPostfixConverter {


  // Data Fields
  /** The operator stack */
  private Stack < Character > operatorStack;

  /** The operators */
  private static final String OPERATORS = "+-*/()^";

  /** The precedence of the operators, matches order of OPERATORS. */
  private static final int[] PRECEDENCE = {
      1, 1, 2, 2, -1, -1, 3};

  /** The postfix string */
  private StringBuilder postfix;

  /** Convert a string from infix to postfix.
      @param infix The infix expression
 * @throws GITEmptyStackException 
 * @throws GITInvalidOperatorException 
      @throws SyntaxErrorException
   */
  public String convert(String infix) throws GITSyntaxErrorException, GITEmptyStackException, GITInvalidOperatorException {
    operatorStack = new Stack < Character > ();
    postfix = new StringBuilder();
    StringTokenizer infixTokens = new StringTokenizer(infix);
    try {
      // Process each token in the infix string.
      while (infixTokens.hasMoreTokens()) {
        String nextToken = infixTokens.nextToken();
        char firstChar = nextToken.charAt(0);
        // Is it an operand?
        if (Character.isJavaIdentifierStart(firstChar)
            || Character.isDigit(firstChar)) {
          postfix.append(nextToken);
          postfix.append(' ');
        } // Is it an operator?
        else if (isOperator(firstChar)) {
        	if(nextToken.length() != 1)
        		throw new GITExceptions.GITInvalidOperatorException
        		("Invalid operator: "+nextToken);
          processOperator(firstChar);
        }
        else {
          throw new GITExceptions.GITSyntaxErrorException
              ("Unexpected Character Encountered: "
               + firstChar);
        }
      } // End while.
      // Pop any remaining operators
      // and append them to postfix.
      while (!operatorStack.empty()) {
        char op = operatorStack.pop();
        // Any '(' on the stack is not matched.
        if (op == '(')
          throw new GITExceptions.GITSyntaxErrorException(
              "Unmatched opening parenthesis");
        postfix.append(op);
        postfix.append(' ');
      }
      // assert: Stack is empty, return result.
      return postfix.toString();
    }
    catch (EmptyStackException ex) {
      throw new GITExceptions.GITEmptyStackException
          ("Syntax Error: The stack is empty");
    }
  }
  
	public String convert(String[] tokens) throws GITSyntaxErrorException,
			GITEmptyStackException, GITInvalidOperatorException {
	  StringBuilder infix = new StringBuilder();
	  for(String token: tokens)
		  infix.append(token).append(" ");
	  return convert(infix.toString());
  }

  /** Method to process operators.
      @param op The operator
      @throws EmptyStackException
   */
  private void processOperator(char op) {
    if (operatorStack.empty() || op == '(') {
      operatorStack.push(op);
    }
    else {
      // Peek the operator stack and
      // let topOp be the top operator.
      char topOp = operatorStack.peek();
      if (precedence(op) > precedence(topOp)) {
        operatorStack.push(op);
      }
      else {
        // Pop all stacked operators with equal
        // or higher precedence than op.
        while (!operatorStack.empty()
               && precedence(op) <= precedence(topOp)) {
          operatorStack.pop();
          if (topOp == '(') {
            // Matching '(' popped - exit loop.
            break;
          }
          postfix.append(topOp);
          postfix.append(' ');
          if (!operatorStack.empty()) {
            // Reset topOp.
            topOp = operatorStack.peek();
          }
        }

        // assert: Operator stack is empty or
        //         current operator precedence >
        //         top of stack operator precedence.
        if (op != ')')
          operatorStack.push(op);
      }
    }
  }

  /** Determine whether a character is an operator.
      @param ch The character to be tested
      @return true if ch is an operator
   */
  public static boolean isOperator(char ch) {
    return OPERATORS.indexOf(ch) != -1;
  }

  /** Determine the precedence of an operator.
      @param op The operator
      @return the precedence
   */
  private int precedence(char op) {
    return PRECEDENCE[OPERATORS.indexOf(op)];
  }
}

