/*
 * File:    Customers.java
 *
 * Course:  CSE222
 * Project: HW01
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */
package com.furkantektas.assignments.cse222.hw01;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Furkan Tektas<tektasfurkan@gmail.com>
 * @version 1.0
 * @created 14-Mar-2013 17:33:16
 */
public class Customers implements ListInterface, Serializable {

	private static final long serialVersionUID = -5736359443525654913L;
	private static final String FILE = "customers.bin";
	private List<Customer> m_list;
	private int m_currentId = 0;

	public Customers() {
		m_list = new ArrayList<Customer>();
	}

	/**
	 * 
	 * @param c
	 *            c
	 */
	public void add(Customer c) {
		m_currentId++;
		c.setID(m_currentId);
		m_list.add(c);
	}

	/**
	 * 
	 * @param pos
	 *            pos
	 */
	public Customer get(int pos) {
		return m_list.get(pos - 1);
	}

	public void list() {
		System.out.printf("%10s\t%30s\t%10s\n", "ID", "CUSTOMER NAME", "BALANCE");
		StringBuilder hLine = new StringBuilder(40);
		for (int i = 0; i < 60; ++i)
			hLine.append('-');
		System.out.println(hLine);

		for (Customer c : m_list)
			System.out.printf("%10d\t%30s\t%10.2f\n", c.getID(), c.getName(), c.getAccount().getBalance());
	}

	/**
	 * Loads the the previously saved data from file.
	 */
	public boolean load() {
		FileInputStream fileStream;
		try {
			fileStream = new FileInputStream(FILE);
		
			ObjectInputStream inputStream = new ObjectInputStream(fileStream);
			Object obj = inputStream.readObject();

			if(obj != null && obj instanceof List) {
				m_list = (List<Customer>) obj;
				//restoring id

				m_currentId = m_list.get(m_list.size()-1).getID();
			}
			
			inputStream.close();
			fileStream.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			return false;
		} 
		return true;
	}

	/**
	 * Saves the list to the FILE.
	 */
	public boolean save() {
		FileOutputStream fileStream;
		ObjectOutputStream outputStream;
		try {
			fileStream = new FileOutputStream(FILE);
			outputStream = new ObjectOutputStream(fileStream);
			outputStream.writeObject((ArrayList<Customer>)m_list);
			outputStream.close();
			fileStream.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	/**
	 * Removes item form list and returns the removed object.
	 * 
	 * @param item
	 */
	public Customer remove(Object item) {
		m_list.remove(item);
		return (Customer) item;
	}

	/**
	 * Returns the number of elements in the list.
	 */
	public int getNum() {
		return m_list.size();
	}
}// end Customers