/*
 * File:    ListInterface.java
 *
 * Course:  CSE222
 * Project: HW01
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */
package com.furkantektas.assignments.cse222.hw01;

import java.io.Serializable;

/**
 * @author Furkan
 * @version 1.0
 * @created 17-Mar-2013 16:20:39
 */
public interface ListInterface extends Serializable{

	/**
	 * Returns the position-th element
	 * 
	 * @param position
	 */
	public Object get(int position);

	/**
	 * List the items in appropriate style.
	 */
	public void list();

	public boolean load();

	/**
	 * Removes item form list and returns the removed object.
	 * 
	 * @param item
	 */
	public Object remove(Object item);

	public boolean save();

	/**
	 * Returns the number of elements in the list.
	 */
	public int getNum();

}