/*
 * File:    Offices.java
 *
 * Course:  CSE222
 * Project: HW01
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */
package com.furkantektas.assignments.cse222.hw01;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;



/**
 * @author Furkan Tektas<tektasfurkan@gmail.com>
 * @version 1.0
 * @created 14-Mar-2013 17:33:16
 */
public class Offices implements ListInterface, Serializable{

	private static final long serialVersionUID = -3859503353222776430L;
	private static final String FILE = "offices.bin";
	private List<Office> m_list;
	private int m_currentId = 0;


	public Offices(){
		m_list = new ArrayList<Office>();
	}

	/**
	 * 
	 * @param o    o
	 */
	public void add(Office o){
		m_currentId++;
		o.setID(m_currentId);
		m_list.add(o);
	}

	/**
	 * 
	 * @param pos    pos
	 */
	public Office get(int pos){
		return m_list.get(pos-1);
	}

	public void list(){
		System.out.printf("%10s\t%30s\n","ID", "OFFICE NAME");
		StringBuilder hLine = new StringBuilder(40);
		for(int i = 0; i < 60; ++i) 
			hLine.append('-');
		System.out.println(hLine);
		
		for(Office o : m_list)
			System.out.printf("%10d\t%30s\n",o.getID(), o.getName());
	}

	/**
	 * Loads the the previously saved data from file.
	 */
	public boolean load() {
		FileInputStream fileStream;
		try {
			fileStream = new FileInputStream(FILE);
		
			ObjectInputStream inputStream = new ObjectInputStream(fileStream);
			Object obj = inputStream.readObject();

			if(obj != null && obj instanceof List) {
				m_list = (List<Office>) obj;
				//restoring id

				m_currentId = m_list.get(m_list.size()-1).getID();
			}
			
			inputStream.close();
			fileStream.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			return false;
		} 
		return true;
	}

	/**
	 * Saves the list to the FILE.  
	 */
	public boolean save() {
		FileOutputStream fileStream = null;
		ObjectOutputStream outputStream = null;
		try {
			fileStream = new FileOutputStream(FILE);
			outputStream = new ObjectOutputStream(fileStream);
			outputStream.writeObject(m_list);
			outputStream.close();
			fileStream.close();
		} catch (Exception e) {
//			e.printStackTrace();
			return false;
		} 

		return true;
	}

	/**
	 * Removes item form list and returns the removed object.
	 * 
	 * @param item
	 */
	public Office remove(Object item){
		m_list.remove(item);
		return (Office) item;
	}
	
	/**
	 * Returns the number of elements in the list.
	 */
	public int getNum(){
		return m_list.size();
	}
	
}//end Offices