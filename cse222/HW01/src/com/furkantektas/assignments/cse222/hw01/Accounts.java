/*
 * File:    Accounts.java
 *
 * Course:  CSE222
 * Project: HW01
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */
package com.furkantektas.assignments.cse222.hw01;


import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;



/**
 * @author Furkan Tektas<tektasfurkan@gmail.com>
 * @version 1.0
 * @created 14-Mar-2013 17:33:15
 */
public class Accounts implements Serializable{

	private static final String FILE = "acounts.bin";
	private Map<Customer,Account> m_list;


	public Accounts(){
		m_list = new HashMap<Customer, Account>();
	}

	/**
	 * 
	 * @param c
	 * @param a    a
	 */
	public void add(Customer c, Account a){
		m_list.put(c, a);
	}

	/**
	 * 
	 * @param c    pos
	 */
	public Account get(Customer c){
		return m_list.get(c);
	}


	/**
	 * 
	 * @param c    index
	 */
	public void remove(Customer c){
		m_list.remove(c);
	}


	/**
	 * List the items in appropriate style.
	 */
	public void list(){

	}

	/**
	 * Removes item form list and returns the removed object.
	 * 
	 * @param item
	 */
	public Account remove(Object item){
		return null;
	}

	/**
	 * Returns the number of elements in the list.
	 */
	public int getNum(){
		return m_list.size();
	}
	
}//end Accounts