/*
 * File:    Driver.java
 *
 * Course:  CSE222
 * Project: HW01
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */
package com.furkantektas.assignments.cse222.hw01;


/**
 * @author Furkan Tektas<tektasfurkan@gmail.com>
 * @version 1.0
 * @created 14-Mar-2013 17:33:16
 */
public class Driver {
	/**
	 * 
	 * @param args    args
	 */
	public static void main(String[] args){
		UI ui = new UI();
		ui.showMainMenu();
	}
}//end Driver