/*
 * File:    Office.java
 *
 * Course:  CSE222
 * Project: HW01
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */
package com.furkantektas.assignments.cse222.hw01;

import java.io.Serializable;
import java.util.List;



/**
 * Bank's branch office class.
 * @author Furkan Tektas<tektasfurkan@gmail.com>
 * @version 1.0
 * @updated 17-Mar-2013 16:42:52
 */
public class Office implements Serializable {

	private static final long serialVersionUID = 3129158545241028231L;
	private List<Worker> m_list;
	private String m_name;
	private Integer m_id;

	/**
	 * 
	 * @param name    name
	 */
	public Office(String name){
		setName(name);
	}

	public String getName(){
		return m_name;
	}

	public List<Worker> getWorkers(){
		return m_list;
	}

	/**
	 * 
	 * @param name
	 */
	public void setName(String name){
		m_name = name;
	}
	
	public void setID(Integer id) {
		m_id = id;
	}
	
	public Integer getID() {
		return m_id;
	}
}//end Office