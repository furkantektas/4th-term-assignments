/*
 * File:    UI.java
 *
 * Course:  CSE222
 * Project: HW01
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */

package com.furkantektas.assignments.cse222.hw01;

import java.util.InputMismatchException;
import java.util.Scanner;



/**
 * @author Furkan Tektas<tektasfurkan@gmail.com>
 * @version 1.0
 * @created 14-Mar-2013 17:33:16
 */
public class UI {
	
	private Customer m_user = null;

	private Accounts m_accounts;
	private Customers m_customers;
	private Offices m_offices;
	private Workers m_workers;
	private Scanner in = new Scanner(System.in);




	public void finalize() throws Throwable {
		save();
		in.close();
		super.finalize();
	}
	
	public UI(){
		m_accounts = new Accounts();
		m_customers = new Customers();
		m_workers = new Workers();
		m_offices = new Offices();
		
		load();
	}

	/**
	 * Creates a new account with owner and adds this account
	 * to m_accounts.
	 * @param owner
	 * @return
	 */
	public Account createAccount(Customer owner){
		Account acc = new Account(owner);
		m_accounts.add(owner, acc);
		return acc;
	}

	/**
	 * Creates a new Customer with user-given name and
	 * 0.0 balanced account. Adds this customer to m_customers.
	 * @return
	 */
	public Customer createCustomer(){	
		System.out.println("Enter the name of customer: ");
		String name = in.nextLine().trim();
		
		
		String password = null;
		do {
			System.out.println("Enter the password: ");
			password = in.nextLine().trim();
			
			if(password.length() < 3)
				System.out.println("Password should contain at least 3 characters.");
		} while(password.length() < 3);
				
		Customer cus = new Customer(name, password);
		Account acc = createAccount(cus);
		cus.setAccount(acc);
		
		m_customers.add(cus);
				
		return cus;
	}

	public Office createOffice(){
		System.out.println("Enter the name of office: ");
		String name = in.nextLine().trim();
		
		Office off = new Office(name);
		
		m_offices.add(off);
		return off;
	}

	public Worker createWorker(){	
		if(m_offices.getNum() < 1) {
			System.out.println("No offices found. In order to create worker, you must create a office first.");
			return null;
		}
		
		System.out.println("Enter the name of worker: ");
		String name = in.nextLine().trim();
		
		
		String password = null;
		do {
			System.out.println("Enter the password: ");
			password = in.nextLine().trim();
			
			if(password.length() < 3)
				System.out.println("Password should contain at least 3 characters.");
		} while(password.length() < 3);
		
		Worker worker = new Worker(name,password);
		Account acc = createAccount(worker);
		worker.setAccount(acc);
		
		listOffices();
		System.out.println("Please select the id of the worker's office: ");
		Office off = m_offices.get(in.nextInt());
		in.nextLine();
		
		worker.setOffice(off);
		
		m_workers.add(worker);
		
		return worker;
	}

	/**
	 * 
	 * @param a    a
	 */
	public void deleteAccount(Account a){

	}

	/**
	 * 
	 * @param c    c
	 */
	public void deleteCustomer(Customer c){

	}

	/**
	 * 
	 * @param o    o
	 */
	public void deleteOffice(Office o){

	}

	/**
	 * 
	 * @param w    w
	 */
	public void deleteWorker(Worker w){

	}

	public void listAccounts(){
		
	}

	public void ListCustomers(){
		m_customers.list();
	}

	public void listOffices(){
		m_offices.list();
	}

	public void listWorkers(){
		m_workers.list();
	}

	/**
	 * Restores the last sessions' data
	 * @return
	 */
	public boolean load(){
		boolean loadCustomers, loadWorkers, loadOffices;
		
		loadCustomers = m_customers.load();
		// Restoring the customers' accounts
		if(loadCustomers) {
			int num = m_customers.getNum();
			Customer c;
			for(int i = 0; i< num; ++i) {
				c = m_customers.get(i+1);
				m_accounts.add(c, c.getAccount());
			}
		} else {
			m_customers = new Customers();
		}
		
		loadWorkers = m_workers.load();
		// Restoring the customers' accounts
		if(loadWorkers) {
			int num = m_workers.getNum();
			Worker w;
			for(int i = 0; i< num; ++i) {
				w = m_workers.get(i+1);
				m_accounts.add(w, w.getAccount());
			}
		} else {
			m_workers = new Workers();
		}
		
		loadOffices = m_offices.load();
		// Restoring the offices
		if(!loadOffices) 
			m_offices = new Offices();
		
		return loadOffices && loadCustomers && loadWorkers;
	}

	public boolean save(){
		return m_customers.save() &
				m_offices.save() & m_workers.save();
	}

	public void showCreateMenu(){
		int userInput = 0;
		do {
			System.out.println("1 - Create Customer");
			System.out.println("2 - Create Worker");
			System.out.println("3 - Create Office");
			System.out.println("4 - Return to main menu");
			System.out.println("Select [1-4] : ");
			
			try {
				userInput = in.nextInt();
			} catch(InputMismatchException e) {
				userInput = -1; // will cause an error below.
			} finally {
				in.nextLine();
			}
			
		} while(userInput < 1 || userInput > 4);
		
		
		switch (userInput) {
		case 1:
			createCustomer();
			break;
		case 2:
			createWorker();
			break;
		case 3:
			createOffice();
			break;
		}
	}

	public void showEditMenu(){
		int userInput = 0;
		do {
			System.out.println("1 - Edit Customer");
			System.out.println("2 - Edit Worker");
			System.out.println("3 - Edit Office");
			System.out.println("4 - Return to main menu");
			System.out.println("Select [1-4] : ");
			
			try {
				userInput = in.nextInt();
			} catch(InputMismatchException e) {
				userInput = -1; // will cause an error below.
			} finally {
				in.nextLine();
			}
			
		} while(userInput < 1 || userInput > 4);
		
		
		switch (userInput) {
		case 1:
			updateCustomer();
			break;
		case 2:
			updateWorker();
			break;
		case 3:
			updateOffice();
			break;
		}
	}

	public void showListMenu(){
		int userInput = 0;
		do {
			System.out.println("1 - List Customers");
			System.out.println("2 - List Workers");
			System.out.println("3 - List Offices");
			System.out.println("4 - Return to main menu");
			System.out.println("Select [1-4] : ");
			
			try {
				userInput = in.nextInt();
			} catch(InputMismatchException e) {
				userInput = -1; // will cause an error below.
			} finally {
				in.nextLine();
			}
			
			
		} while(userInput < 1 || userInput > 4);
				
		switch (userInput) {
		case 1:
			m_customers.list();
			break;
		case 2:
			m_workers.list();
			break;
		case 3:
			m_offices.list();
			break;
		}
	}

	
	public void showWorkerMenu() {
		int userInput = 0;
		do {
			do {
				System.out.println("1 - Create Menu");
				System.out.println("2 - Update Menu");
				System.out.println("3 - List Menu");
				System.out.println("4 - Payment Menu");
				System.out.println("5 - Exit");
				System.out.println("Select [1-5] : ");
				
				try {
					userInput = in.nextInt();
				} catch(InputMismatchException e) {
					userInput = -1; // will cause an error below.
				} finally {
					in.nextLine();
				}
				
				if(userInput < 1 || userInput > 5)
					System.out.println("Invalid choice! Try Again.");
				
			} while(userInput < 1 || userInput > 5);
			
			switch (userInput) {
				case 1:
					showCreateMenu();
					break;
				case 2:
					showEditMenu();
					break;
				case 3:
					showListMenu();
					break;
				case 4:
					showPaymentMenu();
				}
			} while(userInput != 5);
	}
	
	public void showCustomerMenu() {
		int userInput = 0;
		do {
			do {
				System.out.println("1 - Update Your Credentials");
				System.out.println("2 - Your Account Info");
				System.out.println("3 - Payment Menu");
				System.out.println("4 - Exit");
				System.out.println("Select [1-4] : ");
				
				try {
					userInput = in.nextInt();
				} catch(InputMismatchException e) {
					userInput = -1; // will cause an error below.
				} finally {
					in.nextLine();
				}
				
				if(userInput < 1 || userInput > 4)
					System.out.println("Invalid choice! Try Again.");
				
			} while(userInput < 1 || userInput > 4);
			
			switch (userInput) {
				case 1:
					updateCustomer(m_user);
					break;
				case 2:
					System.out.printf("%10s\t%30s\t%10s\n", "ID", "NAME", "BALANCE");
					StringBuilder hLine = new StringBuilder(40);
					for (int i = 0; i < 60; ++i)
						hLine.append('-');
					System.out.println(hLine);
					System.out.printf("%10d\t%30s\t%10.2f\n", m_user.getID(), 
							m_user.getName(), m_user.getAccount().getBalance());
					break;
				case 3:
					showPaymentMenu();
					break;
				}
			} while(userInput != 4);
	}
	
	public void showMainMenu(){
		
		// If there is no user, create an admin with password 123
		if(m_customers.getNum() < 1 && m_workers.getNum() < 1) {
			Worker w = new Worker("Admin", "123");
			w.setAccount(createAccount(w));
			w.setOffice(new Office("Base Office"));
			m_workers.add(w);
		}
		
		while(m_user == null) {
			m_user = login();
			if(m_user == null)
				System.out.println("Invalid login credentials. Please try again. Enter negative id for quit.");
		}
		
		if(m_user instanceof Worker)
			showWorkerMenu();
		else
			showCustomerMenu();
		
		save();
	}

	/**
	 * 
	 */
	public void updateCustomer() {
		ListCustomers();
		System.out.printf("Select an id of customer to update: ");
		try {
			Customer c = m_customers.get(in.nextInt());
			in.nextLine();
			updateCustomer(c);
			
		} catch (Exception e) {
			System.out.println("Incorrect id.");
		}
	}
	
	public void updateCustomer(Customer c) {
		String name, temp;
		System.out.println("Enter the new name of customer: (leave blank if you dont want to change)");
		temp = in.nextLine().trim();
		name = (temp.length() < 1) ? c.getName() : temp;
		c.setName(name);
		
		String password = null;
		do {
			System.out.println("Enter the password: (leave blank if you dont want to change)");
			temp= in.nextLine().trim();
			password = (temp.length() < 1) ? c.getPassword() : temp;
			
			
			if(password.length() < 3)
				System.out.println("Password should contain at least 3 characters.");
		} while(password.length() < 3);
				
		c.setPassword(password);
	}

	/**
	 * 
	 */
	public void updateOffice(){
		listOffices();
		System.out.printf("Select an id of the office to update: ");
		try {
			Office o = m_offices.get(in.nextInt());
			in.nextLine();
			
			String name, temp;
			System.out.println("Enter the new name of office: (leave blank if you dont want to change)");
			temp = in.nextLine().trim();
			name = (temp.length() < 1) ? o.getName() : temp;
			o.setName(name);
			
		} catch (Exception e) {
			System.out.println("Incorrect id.");
		}
	}

	/**
	 * 
	 */
	public void updateWorker(){
		listWorkers();
		System.out.printf("Select an id of worker to update: ");
		try {
			Worker w = m_workers.get(in.nextInt());
			in.nextLine();
			
			String name, temp;
			System.out.println("Enter the new name of worker: (leave blank if you dont want to change)");
			temp = in.nextLine().trim();
			name = (temp.length() < 1) ? w.getName() : temp;
			w.setName(name);
			
			String password = null;
			do {
				System.out.println("Enter the password: (leave blank if you dont want to change)");
				temp= in.nextLine().trim();
				temp = in.nextLine().trim();
				password = (temp.length() < 1) ? w.getPassword() : temp;
				
				
				if(password.length() < 3)
					System.out.println("Password should contain at least 3 characters.");
			} while(password.length() < 3);
					
			w.setPassword(password);
			
			listOffices();
			System.out.println("Please select the id of the worker's office: ");
			try {
				Office off = m_offices.get(in.nextInt());
				w.setOffice(off);
			} catch (Exception e) {
				System.out.println("Invalid id. Worker's office did not be chaanged.");
			} finally {
				in.nextLine();
			}
			
		} catch (Exception e) {
			System.out.println("Incorrect id.");
		}
	}
	
	/**
	 * Login process handler. Checks the user given id and password with
	 * instance variables. If there is a match returns the customer,
	 * null if not.
	 * 
	 * @return
	 */
	public Customer login() {
		
		// Getting user id
		System.out.printf("Enter Your Id: ");
		int customerId;
		try {
			customerId = in.nextInt();
		} catch ( InputMismatchException e) {
			System.out.println("ID should be a number.");
			return null;
		} finally {
			in.nextLine();
		}
		
		// Checking whether the login is cancelled.
		if(customerId < 0) {
			System.out.println("Login cancelled. Terminating.");
			System.exit(0);
		}
		
		
		Customer c;
		
		// Getting the corresponding customer
		try {
			c = m_customers.get(customerId);
		} catch( IndexOutOfBoundsException e) {
			try{
				c = m_workers.get(customerId);
			} catch( IndexOutOfBoundsException er) {				
				// No such customer found. Creating
				// a fake customer with an incredibly strong
				// password. No error will be reported to be secure.
				c = new Customer("FakeUser","A3$#BDA3>#$>"); //backdoor :)
			}
		}
		
		// Getting password
		String password;
		System.out.printf("Enter Your Password: ");
		
		// From OS Console, password will be invisible
		if(System.console() != null) {
			char[] passwdArr = System.console().readPassword();
			password = String.valueOf(passwdArr);
		} else {
			// From IDE's console, password will be visible
			password = in.next();
			in.nextLine();
		}
		
		if(!c.verifyPassword(password))
			c = null;
		return c;
	}
	
	public boolean showPaymentMenu() {
		
		ListCustomers();
		// Getting user id
		System.out.printf("Enter the other customer's Id: ");
		int customerId;
		
		try {
			customerId = in.nextInt();
		} catch ( InputMismatchException e) {
			System.out.println("ID should be a number. Payment cancelled. ");
			return false;
		} finally {
			in.nextLine();
		}
		

		Customer to;
		
		// Getting the corresponding customer
		try {
			to = m_customers.get(customerId);
		} catch( IndexOutOfBoundsException e) {
			System.out.printf("No customer found with id %d. Payment cancelled. \n", customerId);
			return false;
		}
		
		System.out.println("Enter the amount of money to transfer");
		
		float amount;
		
		try {
			amount = in.nextFloat();
		} catch (InputMismatchException e) {
			System.out.println("Invalid amount. Payment cancelled. ");
			return false;
		} finally {
			in.nextLine();
		}
		
		return makePayment(to,amount);
				
	}
	
	/**
	 * Makes a payment from own account.
	 * @param amount
	 * @return
	 */
	public boolean makePayment(double amount) {
		return makePayment(m_user, amount);
	}
	
	/**
	 * Makes a payment from/to current users account to "to" account.
	 * If the amount is negative then money will be transferred from m_user
	 * to "to", or vice versa.
	 * @param to
	 * @param amount
	 * @return
	 */
	public boolean makePayment(Customer to, double amount) {
		return m_user.makePayment(to, (float)amount);
	}
}//end UI
