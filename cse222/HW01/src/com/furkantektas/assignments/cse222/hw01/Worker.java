/*
 * File:    WOrker.java
 *
 * Course:  CSE222
 * Project: HW01
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */
package com.furkantektas.assignments.cse222.hw01;

import java.io.Serializable;

/**
 * Bank workers class. Each worker is also  customer of the bank because of their
 * payroll accounts.
 * @author Furkan Tektas<tektasfurkan@gmail.com>
 * @version 1.0
 * @updated 17-Mar-2013 16:42:53
 */
public class Worker extends Customer implements PersonInt, Serializable {

	private static final long serialVersionUID = 5010361227170595715L;
	private Office m_office;


	/**
	 * Creates a worker without account and office.
	 * setOffice and setAccount should be called after
	 * this constructor. 
	 * @param name
	 */
	public Worker(String name, String password) {
		super(name,password);
		setOffice(null);
	}
	
	/**
	 * 
	 * @param name
	 * @param office    office
	 * @param account    account
	 */
	public Worker(String name, String password, Office office, Account account){
		super(name, password, account);
		setOffice(office);
	}

	public Office getOffice(){
		return m_office;
	}

	public boolean isWorker(){
		return true;
	}

	/**
	 * 
	 * @param office    office
	 */
	public void setOffice(Office office){
		m_office = office;
	}


}//end Worker