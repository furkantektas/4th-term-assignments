/*
 * File:    PersonInt.java
 *
 * Course:  CSE222
 * Project: HW01
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */
package com.furkantektas.assignments.cse222.hw01;

import java.io.Serializable;


/**
 * @author Furkan Tektas<tektasfurkan@gmail.com>
 * @version 1.0
 * @created 14-Mar-2013 17:33:16
 */
public interface PersonInt extends Serializable{

	public Integer getID();

	public String getName();

	public boolean isWorker();

	/**
	 * 
	 * @param id
	 */
	public void setID(Integer id);

	/**
	 * 
	 * @param name    name
	 */
	public void setName(String name);

}