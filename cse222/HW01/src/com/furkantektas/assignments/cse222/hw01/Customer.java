/*
 * File:    Customer.java
 *
 * Course:  CSE222
 * Project: HW01
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */
package com.furkantektas.assignments.cse222.hw01;

import java.io.Serializable;

/**
 * General customer class. Every customer has one Account and each worker is a
 * customer, too.
 * @author Furkan Tektas<tektasfurkan@gmail.com>
 * @version 1.0
 * @updated 17-Mar-2013 16:42:52
 */
public class Customer implements PersonInt,Serializable {

	private static final long serialVersionUID = 6483801524715868863L;
	private Account m_account = null;
	private String m_name;
	private String m_password;

	private Integer m_id;
	
	/**
	 * Creates a new customer with null account.
	 * setAccount should be called after this constructor.
	 * @param name
	 */
	public Customer(String name, String password) {
		this(name,password,null);
	}
	
	/**
	 * 
	 * @param name
	 * @param account    account
	 */
	public Customer(String name, String password, Account account){
		setName(name);
		setPassword(password);
		setAccount(account);
	}

	public Account getAccount(){
		return m_account;
	}

	public Integer getID(){
		return m_id;
	}

	public String getName(){
		return m_name;
	}

	public boolean isWorker(){
		return false;
	}

	/**
	 * 
	 * @param account    a
	 */
	public void setAccount(Account account){
		m_account = account;
	}

	/**
	 * 
	 * @param id
	 */
	public void setID(Integer id){
		m_id = id;
	}

	/**
	 * 
	 * @param name    name
	 */
	public void setName(String name){
		m_name = name;
	}
	
	public String getPassword() {
		return m_password;
	}

	public void setPassword(String password) {
		m_password = password;
	}
	
	public boolean verifyPassword(String password) {
		return m_password.equals(password);
	}
	
	/**
	 * Transfers customers' fund to each other. If the amount is negative,
	 * then the current customer's money will be transferred to other or 
	 * vice versa.
	 * @param other
	 * @param amount
	 * @return
	 */
	public boolean makePayment(Customer other, float amount) {
		Customer from, to;
		
		if(amount < 0f) {
			from = other;
			to = this;
		} else {
			from = this;
			to = other;			
		}
		
		// Self payment	
		if(to != from) {
			// transferring money from current customer to other.
			if((from.getAccount().getBalance()-amount) < 0f) {
				System.out.println("You have no enough money to make this payment.");
				return false;
			}

		
			from.getAccount().updateBalance(-amount);
		}
		to.getAccount().updateBalance(amount);
		
		System.out.println("Payment was done.");
		return true;
	}
}//end Customer