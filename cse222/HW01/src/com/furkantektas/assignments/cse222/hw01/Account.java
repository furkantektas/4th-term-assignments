/*
 * File:    Account.java
 *
 * Course:  CSE222
 * Project: HW01
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */
package com.furkantektas.assignments.cse222.hw01;

import java.io.Serializable;


/**
 * Customer's bank account. Default balance is 0.0.
 * @author Furkan Tektas<tektasfurkan@gmail.com>
 * @version 1.0
 * @updated 17-Mar-2013 16:42:51
 */
public class Account implements Serializable{

	private static final long serialVersionUID = 5998920426773093749L;
	private float m_balance;
	private Customer m_owner;
	public Accounts m_Accounts;

	/**
	 * Initializes new Account object. m_owner should be set
	 * after calling this constructor. 
	 */
	public Account() {
		setBalance(0.0f);
		m_owner = null; 
		
	}
	
	/**
	 * Creates new Account with balance 0.0
	 * @param owner
	 */
	public Account(Customer owner){
		m_owner = owner;
		setBalance(0.0f);
	}
	
	/**
	 * 
	 * @param owner
	 * @param balance    balance
	 */
	public Account(Customer owner, float balance){
		m_owner = owner;
		setBalance(balance);
	}

	public float getBalance(){
		return m_balance;
	}

	public Customer getOwner(){
		return m_owner;
	}

	/**
	 * 
	 * @param balance    balance
	 */
	public void setBalance(float balance){
		m_balance = balance;
	}
	
	/**
	 * Sets the owner if the m_owner is null.
	 * Returns true if the m_Owner was null and the 
	 * parameter owner was set as a m_owner. Returns false
	 * if there is already an owner and parameter owner will not be set
	 * as m_owner.
	 * @param owner
	 * @return
	 */
	public boolean setOwner(Customer owner) {
		if(m_owner != null)
			return false;
		m_owner = owner;
		return true;
	}
	
	/**
	 * Adds the amount to the balance. If the balance will be negative,
	 * process won't be done and the current bill will be returned. 
	 * @param amount
	 * @return current balance
	 */
	public float updateBalance(float amount) {
		if((getBalance()+amount) > 0f)
			setBalance(getBalance()+amount);
		return getBalance();
	}
}//end Account