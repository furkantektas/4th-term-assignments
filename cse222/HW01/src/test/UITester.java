/*
 * File:    UITester.java
 *
 * Course:  CSE222
 * Project: HW01
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */
package test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.furkantektas.assignments.cse222.hw01.Customer;
import com.furkantektas.assignments.cse222.hw01.Office;
import com.furkantektas.assignments.cse222.hw01.UI;
import com.furkantektas.assignments.cse222.hw01.Worker;


public class UITester {

	UI uiObj = new UI();
	
	Customer customer = new Customer("test user", "123");
	Office office = new Office("Test Office");
	Worker worker = new Worker("Test Worker", "123");
	
	public UITester() {
		worker.setOffice(office);
		worker.setAccount(uiObj.createAccount(worker));
	}
	
	@Test
	public void testCreateAccount() {
	    assertEquals("Hesaba set edilen owner yanlis", customer, uiObj.createAccount(customer).getOwner());
	}
	
	

}
