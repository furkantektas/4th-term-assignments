/*
 * File:    WorkerTester.java
 *
 * Course:  CSE222
 * Project: HW01
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */
package test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.furkantektas.assignments.cse222.hw01.Account;
import com.furkantektas.assignments.cse222.hw01.Office;
import com.furkantektas.assignments.cse222.hw01.Worker;

public class WorkerTester {

	String name = "Tester Name";
	String pass = "123Test";
	Worker w = new Worker(name, pass);
	Account a = new Account(w);
	Office o = new Office("Test Office");
	
	@Test
	public void testIsWorker() {
		assertTrue(w.isWorker());
	}

	@Test
	public void testWorkerStringString() {
		w = new Worker(name, pass);
		assertEquals(name, w.getName());
		assertEquals(pass, w.getPassword());
	}

	@Test
	public void testWorkerStringStringOfficeAccount() {
		w = new Worker(name, pass, o, a);
		assertEquals(name, w.getName());
		assertEquals(pass, w.getPassword());
		assertEquals(o, w.getOffice());
	}

	@Test
	public void testGetOffice() {
		w.setOffice(o);
		assertEquals(o, w.getOffice());
	}

	@Test
	public void testSetOffice() {
		w.setOffice(o);
		assertEquals(o, w.getOffice());
	}

}
