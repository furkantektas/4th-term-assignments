/*
 * File:    CustomerTester.java
 *
 * Course:  CSE222
 * Project: HW01
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */
package test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.furkantektas.assignments.cse222.hw01.Account;
import com.furkantektas.assignments.cse222.hw01.Customer;

public class CustomerTester {
	
	String name = "Tester Name";
	String pass = "123Test";
	Customer c = new Customer(name, pass);
	Account a = new Account(c);
	
	@Test
	public void testCustomerStringString() {
		c = new Customer(name, pass);
		assertEquals(name, c.getName());
		assertEquals(pass, c.getPassword());
	}

	@Test
	public void testCustomerStringStringAccount() {
		c = new Customer(name, pass, a);
		assertEquals(a, c.getAccount());
	}

	@Test
	public void testGetAccount() {
		c = new Customer(name, pass, a);
		assertEquals(a, c.getAccount());
	}

	@Test
	public void testGetName() {
		c = new Customer(name, pass, a);
		assertEquals(name, c.getName());
	}

	@Test
	public void testIsWorker() {
		assertFalse(c.isWorker());
	}

	@Test
	public void testSetAccount() {
		c.setAccount(a);
		assertEquals(a, c.getAccount());
	}

	@Test
	public void testSetName() {
		c.setName(name);
		assertEquals(name, c.getName());
	}

	@Test
	public void testGetPassword() {
		c.setPassword(pass);
		assertEquals(pass, c.getPassword());
	}

	@Test
	public void testSetPassword() {
		c.setPassword(pass);
		assertEquals(pass, c.getPassword());
	}

	@Test
	public void testVerifyPassword() {
		c.setPassword(pass);
		assertTrue(c.verifyPassword(pass));
		assertFalse(c.verifyPassword(name));
	}

}
