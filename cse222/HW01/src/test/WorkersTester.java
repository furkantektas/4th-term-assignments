/*
 * File:    WorkersTester.java
 *
 * Course:  CSE222
 * Project: HW01
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */
package test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.furkantektas.assignments.cse222.hw01.Account;
import com.furkantektas.assignments.cse222.hw01.Customers;
import com.furkantektas.assignments.cse222.hw01.Office;
import com.furkantektas.assignments.cse222.hw01.Worker;
import com.furkantektas.assignments.cse222.hw01.Workers;

public class WorkersTester {

	Workers list = new Workers();
	Office o = new Office("Test Office");
	Account a = new Account();
	Worker w = new Worker("Tester Name", "Tester Pass", o, a);
	
	@Test
	public void testWorkers() {
		list.add(w);
		assertEquals(w, list.get(list.getNum()));
	}

	@Test
	public void testGet() {
		list.add(w);
		assertEquals(w, list.get(list.getNum()));
	}

	@Test
	public void testLoad() {
		while(list.getNum() > 0) 
			list.remove(list.getNum());
		list.add(w);
		list.save();
		Customers list2 = new Customers();
		
		assertTrue(list2.load());
		assertEquals(list.getNum(), list2.getNum());
	}

	@Test
	public void testSave() {
		while(list.getNum() > 0) 
			list.remove(list.getNum());
		list.add(w);
		assertTrue(list.save());
	}

	@Test
	public void testRemove() {
		int size1 = list.getNum();
		list.add(w);
		list.remove(w);
		int size2 = list.getNum();
		assertTrue(size1 == size2);
	}

	@Test
	public void testGetNum() {
		list.add(w);
		int size1 = list.getNum();
		list.add(w);
		int size2 = list.getNum();
		assertTrue((size1 + 1) == size2);
		
	}

}
