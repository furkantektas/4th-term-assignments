/*
 * File:    OfficeTester.java
 *
 * Course:  CSE222
 * Project: HW01
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */
package test;
import static org.junit.Assert.*;

import org.junit.Test;

import com.furkantektas.assignments.cse222.hw01.Office;


public class OfficeTester {

	
	@Test
	public void test() {
		assertEquals("Test Name", new Office("Test Name").getName());
	}
	
	

}
