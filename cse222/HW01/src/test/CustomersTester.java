/*
 * File:    CustomersTester.java
 *
 * Course:  CSE222
 * Project: HW01
 *
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */
package test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.furkantektas.assignments.cse222.hw01.Customer;
import com.furkantektas.assignments.cse222.hw01.Customers;

public class CustomersTester {

	Customers list = new Customers();
	Customer c = new Customer("Tester Name", "Tester Pass");
	
	@Test
	public void testAdd() {
		list.add(c);
		assertEquals(c, list.get(list.getNum()));
	}

	@Test
	public void testGet() {
		list.add(c);
		assertEquals(c, list.get(list.getNum()));
	}

	@Test
	public void testLoad() {
		while(list.getNum() > 0) 
			list.remove(list.getNum());
		list.add(c);
		list.save();
		Customers list2 = new Customers();
		
		assertTrue(list2.load());
		assertEquals(list.getNum(), list2.getNum());
	}

	@Test
	public void testSave() {
		while(list.getNum() > 0) 
			list.remove(list.getNum());
		list.add(c);
		assertTrue(list.save());
		
	}

	@Test
	public void testRemove() {
		int size1 = list.getNum();
		list.add(c);
		list.remove(c);
		int size2 = list.getNum();
		assertTrue(size1 == size2);
	}

	@Test
	public void testGetNum() {
		list.add(c);
		int size1 = list.getNum();
		list.add(c);
		int size2 = list.getNum();
		assertTrue((size1 + 1) == size2);
		
	}

}
