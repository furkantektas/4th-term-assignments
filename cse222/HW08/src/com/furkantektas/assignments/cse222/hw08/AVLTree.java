package com.furkantektas.assignments.cse222.hw08;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * BST is an ArrayList based binary search tree implementation.
 * 
 * @author Furkan Tektas<tektasfurkan@gmail.com> Student Number : 111044029
 * 
 * @param <E>
 *            - Any class. If no appropriate comparator is given, then class
 *            should be Comparable.
 */
public class AVLTree<E extends Comparable<E>> implements Cloneable,
		Serializable, BinaryTree<E>, SearchTree<E> {

	private static final long serialVersionUID = 414277996910013563L;

	private int size = 0;

	private List<E> data = new ArrayList<E>();
	private List<Integer> balanceList = new ArrayList<Integer>();

	private final Comparator<? super E> comparator;

	public AVLTree() {
		this(null);
	}

	public AVLTree(Comparator<? super E> comparator) {
		this.comparator = comparator;
	}

	@Override
	public boolean add(E item) {
		return add(item, 0);
	}

	private boolean add(E item, int index) {

		if (isEmpty()) {
			data.add(item);
			++size;
			return true;
		}

		if (contains(item))
			return false;

		int compareResult;
		int balanceChange = 0;
		while (rangeCheck(index)) {

			if (!rangeCheck(index) || data.get(index) == null)
				break;

			compareResult = compare(data.get(index), item);

			if (compareResult < 0) {
				// check left child
				index = getLeftChildIndex(index);
				balanceChange = 1;
			} else if (compareResult > 0) {
				// check right child
				index = getRightChildIndex(index);
				balanceChange = -1;
			}
		}

		resize(index);
		data.set(index, item); // check size
		setBalance(getParentIndex(index), getBalance(getParentIndex(index))
				+ balanceChange);
		updateBalances(getParentIndex(getParentIndex(index)));
		++size;
		// balance(getParentIndex(index));
		return true;
	}

	@Override
	public boolean contains(E target) {
		return (find(target) != null);
	}

	@Override
	public E find(E target) {
		int index = findIndex(target);

		if (rangeCheck(index))
			return data.get(index);
		return null;
	}

	/**
	 * Returns the index of the target if its found. Otherwise returns -1.
	 * 
	 * @param target
	 * @return
	 */
	private int findIndex(E target) {
		int index = 0;
		int compareResult;

		if (target == null)
			return -1;

		while (rangeCheck(index)) {
			if (data.get(index) == null)
				return -1;

			compareResult = compare(data.get(index), target);

			if (compareResult == 0)
				return index;
			else if (compareResult < 0) // check left child
				index = getLeftChildIndex(index);
			else if (compareResult > 0) // check right child
				index = getRightChildIndex(index);
		}

		if (data.size() <= index)
			return -1;
		return index;
	}

	/**
	 * Returns the biggest element of subtree beginning of index.
	 * 
	 * @param index
	 * @return
	 */
	private E findMax(int index) {
		int maxIndex;

		if (rangeCheck(getLeftChildIndex(index))
				&& data.get(getLeftChildIndex(index)) != null)
			maxIndex = findMaxIndex(getLeftChildIndex(index));
		else
			maxIndex = findMaxIndex(getRightChildIndex(index));

		if (rangeCheck(maxIndex))
			return data.get(maxIndex);
		return null;
	}

	/**
	 * Returns the biggest element's index of subtree beginning of index.
	 * 
	 * @param index
	 * @return
	 */
	private int findMaxIndex(int index) {
		int biggestChildIndex = index;

		if (data.size() <= index || data.get(index) == null)
			return index;

		biggestChildIndex = findMaxIndex(getRightChildIndex(biggestChildIndex));

		if (data.size() <= biggestChildIndex
				|| data.get(biggestChildIndex) == null
				|| (compare(data.get(index), data.get(biggestChildIndex)) <= 0))
			return index;
		return biggestChildIndex;
	}

	/**
	 * Returns the smallest element of subtree beginning of index.
	 * 
	 * @param index
	 * @return
	 */
	@SuppressWarnings("unused")
	private E findMin(int index) {
		int minIndex;
		if (rangeCheck(getRightChildIndex(index))
				&& data.get(getRightChildIndex(index)) != null)
			minIndex = findMinIndex(getRightChildIndex(index));
		else
			minIndex = findMinIndex(getLeftChildIndex(index));

		if (rangeCheck(minIndex))
			return data.get(minIndex);
		return null;
	}

	/**
	 * Returns the smallest element's index of subtree beginning of index.
	 * 
	 * @param index
	 * @return
	 */
	private int findMinIndex(int index) {
		int smallestChildIndex = index;

		if (data.size() <= index || data.get(index) == null)
			return index;

		smallestChildIndex = findMinIndex(getLeftChildIndex(smallestChildIndex));

		if (data.size() <= smallestChildIndex
				|| data.get(smallestChildIndex) == null
				|| (compare(data.get(index), data.get(smallestChildIndex)) >= 0))
			return index;
		return smallestChildIndex;
	}

	@Override
	public E delete(E target) {
		if (deleteRecursive(target) != null) {
			--size;
			return target;
		}
		return null;
	}

	/**
	 * Deletes and regenerates the new structure of the tree.
	 * 
	 * @param target
	 *            - element to be removed.
	 * @return
	 */
	private E deleteRecursive(E target) {
		int index = findIndex(target), rIndex = getRightChildIndex(index), lIndex = getLeftChildIndex(index);

		if (rangeCheck(index)) {
			if (isLeaf(index))
				data.set(index, null);
			else if (!isFilled(lIndex)) {
				shiftUpTree(rIndex);
			} else {
				data.set(index, deleteRecursive(findMax(index)));
				return target;
			}
		}
		return target;
	}

	/**
	 * Shifts the tree started with index up one level.
	 * 
	 * @param index
	 *            - Initial index of the subtree.
	 */
	private void shiftUpTree(int index) {
		moveUpSubTree(index, getParentIndex(index));
	}

	@Override
	public boolean remove(E target) {
		return (delete(target) != null);
	}

	@Override
	public BinaryTree<E> getLeftSubtree() {
		return null;
	}

	@Override
	public BinaryTree<E> getRightSubtree() {
		return null;
	}

	@Override
	public E getData() {
		if (!data.isEmpty())
			return data.get(0);
		return null;
	}

	@Override
	public boolean isLeaf() {
		return (size() == 1);
	}

	/**
	 * Returns true if the index-th element has no left and right subtrees.
	 * 
	 * @param index
	 * @return
	 */
	private boolean isLeaf(int index) {
		int rIndex = getRightChildIndex(index), lIndex = getLeftChildIndex(index);

		if (rIndex >= data.size() && lIndex >= data.size())
			return true;
		if (rangeCheck(rIndex) && rangeCheck(lIndex))
			return (data.get(lIndex) == null && data.get(rIndex) == null);
		return false;
	}

	/**
	 * Returns true if the tree is empty.
	 */
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public void clear() {
		data.clear();
		size = 0;
	}

	@Override
	public int size() {
		return size;
	}

	private int depth(int index) {
		int lDepth, rDepth;

		if (!rangeCheck(index))
			return 0;

		lDepth = depth(getLeftChildIndex(index));
		rDepth = depth(getRightChildIndex(index));

		return 1 + Math.max(lDepth, rDepth);
	}

	/**
	 * Expands the list by adding dummy null values until list size reaches the
	 * newSize.
	 * 
	 * @param newSize
	 *            - list's new size
	 */
	private void resize(int newSize) {
		for (int i = data.size(); i <= newSize; ++i) {
			data.add(null);
			balanceList.add(0);
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		print(sb, 0);
		return sb.toString();
	}

	/**
	 * Wrapper method of print.
	 * 
	 * @param sb
	 * @param index
	 */
	private void print(StringBuilder sb, int index) {
		print(sb, "", index);
	}

	/**
	 * Appends the structure of the tree to the sb.
	 * 
	 * @param sb
	 *            - StringBuilder to tree structure be appended.
	 * @param prefix
	 * @param index
	 */
	private void print(StringBuilder sb, String prefix, int index) {
		if (data.size() <= index)
			return;
		sb.append(data.get(index) + "(" + index + ")" + "\n");
		prefix = prefix + "|   ";
		if (data.get(index) != null || true) {
			if (rangeCheck(getLeftChildIndex(index))) {
				sb.append(prefix);
				// sb.append("("+getLeftChildIndex(index)+")");
				sb.append("|---");
				print(sb, prefix, getLeftChildIndex(index));
			}

			if (data.size() > getRightChildIndex(index)) {
				sb.append(prefix);
				sb.append("----");
				print(sb, prefix, getRightChildIndex(index));
			}
		}

	}

	/**
	 * Compares item1 with item2 and returns the comparison result. Uses
	 * comparator if it's not null, uses item1's compareTo() method otherwise.
	 * 
	 * @param item1
	 * @param item2
	 * @return
	 */
	private int compare(E item1, E item2) {
		if (comparator != null)
			return comparator.compare(item1, item2);
		return ((Comparable<? super E>) item2).compareTo(item1);
	}

	/**
	 * Validates the index.
	 * 
	 * @param index
	 * @return
	 */
	private boolean rangeCheck(int index) {
		return (index >= 0 && index < data.size());
	}

	/**
	 * Returns true if the index-th cell is in the range of data and is not null
	 * 
	 * @param index
	 * @return
	 */
	private boolean isFilled(int index) {
		return (rangeCheck(index) && data.get(index) != null);
	}

	private int getParentIndex(int childIndex) {
		// right child
		if ((childIndex % 2) == 0)
			return (childIndex / 2 - 1);
		// left child
		return ((childIndex + 1) / 2 - 1);
	}

	private int getRightChildIndex(int index) {
		return (index + 1) * 2;
	}

	private int getLeftChildIndex(int index) {
		return (index + 1) * 2 - 1;
	}

	private E getRightChild(int index) {
		try {
			return data.get(getRightChildIndex(index));
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	private E getLeftChild(int index) {
		try {
			return data.get(getLeftChildIndex(index));
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	@Override
	public int maxDepth() {
		return depth(0);
	}

	/* ==== AVL TREE OPERATIONS ===== */

	private boolean swap(int index1, int index2) {
		if (!rangeCheck(index1) || !rangeCheck(index2))
			return false;

		E temp = data.get(index1);
		data.set(index1, data.get(index2));
		data.set(index2, temp);
		return true;
	}

	/**
	 * Makes an left rotation on the tree. index is the local root of tree to be
	 * rotated.
	 * 
	 * @param index
	 * @return
	 */
	private boolean rotateLeft(int index) {
		// reached the root.
		if (!rangeCheck(index))
			return false;

		E leftChild = getLeftChild(index);
		E rightChild = getRightChild(index);
		E rightLeftChild = data
				.get(getRightChildIndex(getLeftChildIndex(index)));
		// pushing one level down to right side
		// pushRightOneLevel(index);
		moveDownSubTree(index, getRightChildIndex(index));

		// root.right = root
		data.set(getRightChildIndex(index), data.get(index));

		// root.left.right = root.right.left
		data.set(getLeftChildIndex(getRightChildIndex(index)), rightLeftChild);

		// root = root.left
		data.set(index, leftChild);

		// pull one level up from left side
		shiftUpTree(getLeftChildIndex(index));
		return true;
	}

	/**
	 * Makes an right rotation on the tree. index is the local root of tree to
	 * be rotated.
	 * 
	 * @param index
	 * @return
	 */
	private boolean rotateRight(int index) {
		// reached the root.
		if (!rangeCheck(index))
			return false;

		E leftChild = getLeftChild(index);
		E rightChild = getRightChild(index);

		int leftRightIndex = getRightChildIndex(getLeftChildIndex(index));
		int rightLeftIndex = getLeftChildIndex(getRightChildIndex(index));
		E leftRightChild = data.get(leftRightIndex);

		// pushing one level down to right side
		moveDownSubTree(getRightChildIndex(index),
				getRightChildIndex(getRightChildIndex(index)));

		// root.right = root
		data.set(getRightChildIndex(index), data.get(index));

		// root.left.right = root.right.left
		moveDownSubTree(leftRightIndex, rightLeftIndex);

		// root.left.left = root.left
		shiftUpTree(getLeftChildIndex(index));
		// moveSubTree(getLeftChildIndex(getLeftChildIndex(index)),
		// getLeftChildIndex(index));

		// pushLeftOneLevel(index);
		// pull one level up from left side
		return true;
	}

	/**
	 * Pushes down to left hand side by one level.
	 * 
	 * @param index
	 */
	private void pushLeftOneLevel(int index) {
		moveDownSubTree(index, getLeftChildIndex(index));
		// data.set(LeftChildIndex(index),null);
		eraseSubTree(getRightChildIndex(index));
		// data.set(getRightChildIndex(index), null);
		data.set(index, null);

	}

	/**
	 * Pushes down to right hand side by one level.
	 * 
	 * @param index
	 */
	private void pushRightOneLevel(int index) {
		moveDownSubTree(index, getRightChildIndex(index));
		data.set(getLeftChildIndex(index), null);
		// data.set(getRightChildIndex(index), null);
		data.set(index, null);
	}

	/**
	 * Returns true if the index-th node exists and has at least one child node.
	 * 
	 * @param index
	 * @return
	 */
	private boolean hasChild(int index) {
		return ((rangeCheck(index)) && (isFilled(getLeftChildIndex(index)) 
				|| isFilled(getRightChildIndex(index))));
	}

	/**
	 * Assigns null to all nodes of sub tree and its successors all nodes.
	 * 
	 * @param rootIndex
	 */
	private void eraseSubTree(int rootIndex) {
		if (!rangeCheck(rootIndex))
			return;
		data.set(rootIndex, null);
		eraseSubTree(getLeftChildIndex(rootIndex));
		eraseSubTree(getRightChildIndex(rootIndex));
	}

	/**
	 * Moves sub tree from oldRootIndex to newRootIndex. After this method has
	 * been called, there may be duplicate elements, and such elements should be
	 * erased.
	 * 
	 * @param oldRootIndex
	 * @param newRootIndex
	 */
	private void moveDownSubTree(int oldRootIndex, int newRootIndex) {
		if (!isFilled(oldRootIndex))
			return;

		resize(getRightChildIndex(newRootIndex));

		if (isFilled(getLeftChildIndex(oldRootIndex))) {
			moveDownSubTree(getLeftChildIndex(oldRootIndex),
					getLeftChildIndex(newRootIndex));
			data.set(getLeftChildIndex(newRootIndex),
					getLeftChild(oldRootIndex));
		} else {
			// assigning null to new tree's child nodes.
			eraseSubTree(getLeftChildIndex(newRootIndex));
		}

		if (isFilled(getRightChildIndex(oldRootIndex))) {
			moveDownSubTree(getRightChildIndex(oldRootIndex),
					getRightChildIndex(newRootIndex));
			data.set(getRightChildIndex(newRootIndex),
					getRightChild(oldRootIndex));
		} else {
			eraseSubTree(getRightChildIndex(newRootIndex));
		}

		data.set(newRootIndex, data.get(oldRootIndex));

	}

	/**
	 * Moves sub tree from oldRootIndex to newRootIndex. After this method has
	 * been called, there may be duplicate elements, and such elements should be
	 * erased.
	 * 
	 * @param oldRootIndex
	 * @param newRootIndex
	 */
	private void moveUpSubTree(int oldRootIndex, int newRootIndex) {
		if (!isFilled(oldRootIndex))
			return;

		resize(getRightChildIndex(newRootIndex));

		data.set(newRootIndex, data.get(oldRootIndex));
		data.set(getLeftChildIndex(newRootIndex), getLeftChild(oldRootIndex));

		data.set(getRightChildIndex(newRootIndex), getRightChild(oldRootIndex));

		moveUpSubTree(getLeftChildIndex(oldRootIndex),
				getLeftChildIndex(newRootIndex));
		moveUpSubTree(getRightChildIndex(oldRootIndex),
				getRightChildIndex(newRootIndex));
		
//		if (!isFilled(getLeftChildIndex(oldRootIndex))) {
//			eraseSubTree(getLeftChildIndex(newRootIndex));
//		}
//
//		if (!isFilled(getRightChildIndex(oldRootIndex))) {
//			eraseSubTree(getRightChildIndex(newRootIndex));
//		} 

	}

	/**
	 * Balances the AVL tree according to AVL tree rules.
	 * 
	 * @param index
	 */
	private void balance(int index) {
		int parentIndex = getParentIndex(index);

		// reached the root.
		if (!rangeCheck(parentIndex))
			return;

		if (getBalance(parentIndex) < BalanceState.LEFT && 
				getBalance(index) == BalanceState.LEFT) {
			// Left Left tree
			rotateRight(parentIndex);
		} else if (getBalance(parentIndex) < BalanceState.LEFT && 
				getBalance(index) == BalanceState.RIGHT) {
			// Left Right tree
			rotateLeft(index);
			rotateRight(parentIndex);
		} else if (getBalance(parentIndex) > BalanceState.RIGHT && 
				getBalance(index) == BalanceState.RIGHT) {
			// Right Right tree
			rotateLeft(parentIndex);
		} else if (getBalance(parentIndex) > BalanceState.RIGHT && 
				getBalance(index) == BalanceState.LEFT) {
			// Right Left tree
			rotateRight(index);
			rotateLeft(parentIndex);
		} else {
			// local sub tree is balanced, check the successor tree.
			balance(parentIndex);
		}
	}

	/**
	 * Returns the AVL balance of tree. index is the local root of that tree.
	 * 
	 * @param index
	 * @return
	 */
	private int getBalance(int index) {
		if (!rangeCheck(index))
			return 0;
		return balanceList.get(index);
	}

	/**
	 * Sets the AVL balance of tree. index is the local root of that tree.
	 * 
	 * @param index
	 * @param balance
	 */
	private void setBalance(int index, int balance) {
		balanceList.set(index, balance);
	}

	private boolean isBalanced(int index) {
		int balanceState = getBalance(index);
		return (balanceState >= BalanceState.LEFT && 
				balanceState <= BalanceState.RIGHT);
	}

	/**
	 * Calculates and sets the new balance of index and the successors of index.
	 * 
	 * @Pre: balanceList has been updated for successors.
	 * @param index
	 */
	private void updateBalances(int index) {
		if (!rangeCheck(index))
			return;
		setBalance(index, getBalance(getRightChildIndex(index))
				- getBalance(getLeftChildIndex(index)));
		updateBalances(getParentIndex(index));
	}

	private static interface BalanceState {
		static int LEFT = -1;
		static int BALANCED = 0;
		static int RIGHT = 1;
	}

}
