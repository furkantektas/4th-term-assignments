package com.furkantektas.assignments.cse222.hw08;


public interface BinaryTree<E> {

	/**
	 * Returns the left sub-tree.
	 * @return left sub tree, if exists.
	 */
	public BinaryTree<E> getLeftSubtree();
	
	/**
	 * Returns the right sub-tree.
	 * @return right sub tree, if exists.
	 */
	public BinaryTree<E> getRightSubtree();
	
	/**
	 * Return the data in the root
	 * @return root data
	 */
	public E getData();

	/**
	 * Returns true if the tree is a leaf 
	 * @return true if the tree is a leaf
	 */
	public boolean isLeaf();
	
	/**
	 * Returns <code>true</code> if tree is empty, otherwise <code>false</code>.
	 * @return size() == 0
	 */
	public boolean isEmpty();
	
	/**
	 * Removes all the elements of tree.
	 */
	public void clear();
	
	/**
	 * Returns the number of elements in tree.
	 * @return number of elements
	 */
	public int size();
	
}
