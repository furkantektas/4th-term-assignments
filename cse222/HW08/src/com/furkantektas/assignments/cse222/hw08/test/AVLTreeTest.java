package com.furkantektas.assignments.cse222.hw08.test;
import static org.junit.Assert.*;

import org.junit.Test;

import com.furkantektas.assignments.cse222.hw08.AVLTree;


public class AVLTreeTest {

	
	@Test
	public void rootDeleteTestWithTwoChildNode() {
		AVLTree<Integer> AVLTree = new AVLTree<Integer>();
		AVLTree.add(8);
		AVLTree.add(7);
		AVLTree.add(9);
		AVLTree.add(3);
		AVLTree.add(1);
		AVLTree.add(5);
		AVLTree.add(4);
		AVLTree.add(6);
		
		AVLTree.delete(8);
		
		assertNotNull(AVLTree.getData());
		assertEquals(new Integer(7), AVLTree.getData());
	}
	
	@Test
	public void rootDeleteTestOnlyLeftNode() {
		AVLTree<Integer> AVLTree = new AVLTree<Integer>();
		AVLTree.add(8);
		AVLTree.add(7);
		AVLTree.add(6);
		AVLTree.add(5);
		
		AVLTree.delete(8);
		
		assertNotNull(AVLTree.getData());
		assertEquals(new Integer(7), AVLTree.getData());
	}
	
	@Test
	public void rootDeleteTestOnlyRightNode() {
		AVLTree<Integer> AVLTree = new AVLTree<Integer>();
		AVLTree.add(5);
		AVLTree.add(6);
		AVLTree.add(7);
		AVLTree.add(8);
		
		AVLTree.delete(5);
		
		assertNotNull(AVLTree.getData());
		assertEquals(new Integer(6), AVLTree.getData());
	}
	
	@Test
	public void rootDeleteTestWithoutNode() {
		AVLTree<Integer> AVLTree = new AVLTree<Integer>();
		AVLTree.delete(5);
		
		assertEquals(null, AVLTree.getData());
	}
	
	@Test
	public void testSize() {
		AVLTree<Integer> AVLTree = new AVLTree<Integer>();
		AVLTree.add(5);
		AVLTree.add(6);
		AVLTree.add(7);
		
		int oldSize = AVLTree.size();
		AVLTree.delete(5);
		assertTrue(oldSize == 3);
		assertEquals(oldSize, AVLTree.size()+1);
	}
	

	@Test
	public void testClear() {
		AVLTree<Integer> AVLTree = new AVLTree<Integer>();
		AVLTree.add(5);
		AVLTree.add(6);
		AVLTree.add(7);
		
		AVLTree.clear();
		assertEquals(0, AVLTree.size());
	}
	
	@Test
	public void testDepth() {
		AVLTree<Integer> AVLTree = new AVLTree<Integer>();
		AVLTree.add(5);
		AVLTree.add(6);
		AVLTree.add(7);
		
		assertEquals(3, AVLTree.maxDepth());
		AVLTree.clear();
		
		AVLTree.add(8);
		AVLTree.add(7);
		AVLTree.add(6);
		AVLTree.add(5);
		
		assertEquals(4, AVLTree.maxDepth());
	}
	
	@Test
	public void testFind() {
		AVLTree<Integer> AVLTree = new AVLTree<Integer>();
		AVLTree.add(5);
		AVLTree.add(6);
		AVLTree.add(7);
		
		Integer num = new Integer(9);
		AVLTree.add(num);
		
		assertEquals(num, AVLTree.find(num));
	}
	
	@Test
	public void testFindWithStrings() {
		AVLTree<String> AVLTree = new AVLTree<String>();
		AVLTree.add("aba");
		AVLTree.add("caba");
		AVLTree.add("aaa");
		AVLTree.add("aac");
		AVLTree.add("daac");
		
		
		String str = new String("searchTerm");
		AVLTree.add(str);
		
		assertEquals(str, AVLTree.find(str));
	}
	
	
	
	
	
	
	
	

}
