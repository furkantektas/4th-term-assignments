/*
 * File:    UnrolledLinkedList.java
 *
 * Course:  CSE222
 * Project: HW03
 * Created: 23.03.2013 14:46:12
 * 
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */

package com.furkantektas.assignments.cse222.hw03;

import java.io.Serializable;
import java.util.AbstractSequentialList;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class UnrolledLinkedList<E> extends AbstractSequentialList<E> implements
		Serializable, Cloneable {
	

	private static final long serialVersionUID = 7549278501624486253L;

	// The maximum number of elements that can be stored in a single node.
	private static int m_DefaultNodeCapacity = 16;

	// The current size of this list. Not private for performance.
	int m_size = 0;

	// The first node of this list. Not private for performance.
	Node<E> m_firstNode;

	// The last node of this list.Not private for performance.
	Node<E> m_lastNode;

	/**
	 * Constructs an empty list with {@link UnrolledLinkedList#m_DefaultNodeCapacity
	 * m_DefaultNodeCapacity} of 16.
	 */
	public UnrolledLinkedList() {
		this(m_DefaultNodeCapacity);
	}

	/**
	 * Constructs an empty Unrolled Linked List with the specified
	 * {@link UnrolledLinkedList#m_nodeCapacity nodeCapacity}. For performance
	 * reasons <tt>nodeCapacity</tt> must be greater than or equal to 10.
	 * 
	 * @param nodeCapacity
	 *            The maximum number of elements that can be stored in a single
	 *            node.
	 * @throws IllegalArgumentException
	 *             if <tt>nodeCapacity</tt> is less than 10
	 */
	public UnrolledLinkedList(int nodeCapacity) throws IllegalArgumentException {
		if (nodeCapacity <= 10)
			throw new IllegalArgumentException("Node Capacity should be >= 10");

		m_DefaultNodeCapacity = nodeCapacity;
		m_firstNode = new Node<E>(nodeCapacity);
		m_lastNode = m_firstNode;
	}

	
    /**
     * Returns a list-iterator of the elements in this list (in proper
     * sequence), starting at the specified position in the list.
     * Obeys the general contract of {@code List.listIterator(int)}.<p>
     *
     * @param index index of the first element to be returned from the
     *              list-iterator (by a call to {@code next})
     * @return a ListIterator of the elements in this list (in proper
     *         sequence), starting at the specified position in the list
     * @throws IndexOutOfBoundsException {@inheritDoc}
     * @see List#listIterator(int)
     */
	@Override
	public ListIterator<E> listIterator(int index) {
		checkPositionIndex(index);
		return new UListIterator(index);
	}

	@Override
	public int size() {
		return m_size;
	}
	
    /**
     * Returns the first element in this list.
     *
     * @return the first element in this list
     * @throws NoSuchElementException if this list is empty
     */
	public E getFirst() {
		return get(0);
	}
	
    /**
     * Returns the last element in this list.
     *
     * @return the first element in this list
     * @throws NoSuchElementException if this list is empty
     */
	public E getLast() {
		return get(m_size-1);
	}
	
	/**
	 * Adds item to the beginning of the list. 
	 * @param item the element to be added to the beginning.
	 */
	public void addFirst(E item) {
		add(0, item);
	}
	
	/**
	 * Adds item to the end of the list. 
	 * @param item the element to be added to the end.
	 */
	public void addLast(E item) {
		add(m_size, item);
	}
	
	/**
	 * Prints the list elements in the brackets '[]' and 
	 * separated by comma
	 */
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		
		E firstElement = getFirst();
		E lastElement = getLast();
		for(E element : this) { 
			if(element == firstElement)
				str.append('[');
			str.append(element);
			if(element == lastElement)
				str.append(']');
			else
				str.append(", ");
		}
		
		return str.toString();
	}

	/**
     * Returns a shallow copy of this {@code UnrolledLinkedList}. (The elements
     * themselves are not cloned.)
     *
     * @return a shallow copy of this {@code UnrolledLinkedList} instance
     */
    @SuppressWarnings("unchecked")
	public Object clone() {
    	UnrolledLinkedList<E> clone;
    	try {
    		clone = (UnrolledLinkedList<E>) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError();
        }
        
        // Resetting clone
        clone.m_firstNode = clone.m_lastNode = null;
        clone.m_size = 0;

        // Copying our elements to clone
        for (Node<E> x = m_firstNode; x != null; x = x.next) {
        	clone.m_lastNode.next = new Node<E>(x.capacity);
        	clone.m_lastNode.next.prev = clone.m_lastNode;
        	clone.m_lastNode = clone.m_lastNode.next;
        	
        	clone.m_lastNode.size = x.size;
        	System.arraycopy(x.elements, 0, clone.m_lastNode.elements, 0, x.size);
        }

        return clone;
    }
	
    

    /**
     * Tells if the argument is the index of an existing element.
     */
    private boolean isElementIndex(int index) {
        return index < m_size && index >= 0;
    }

    /**
     * Tells if the argument is the index of a valid position for an
     * iterator or an add operation.
     */
    private boolean isPositionIndex(int index) {
        return index <= m_size && index >= 0;
    }

    /**
     * Constructs an IndexOutOfBoundsException detail message.
     * Of the many possible refactorings of the error handling code,
     * this "outlining" performs best with both server and client VMs.
     * @param index index that cause to exception
     * @return
     */
    private String outOfBoundsMsg(int index) {
        return "Index: "+index+", Size: "+m_size;
    }

    /**
     * Returns whether the index is an index of the existing element 
     * or not.
     * @param index index to be checked
     */
    @SuppressWarnings("unused")
	private void checkElementIndex(int index) {
        if (!isElementIndex(index))
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
    }

    /**
     * Returns whether the index is a valid index for iterator or 
     * add operation or not.
     * @param index index to be checked
     */
    private void checkPositionIndex(int index) {
        if (!isPositionIndex(index))
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
    }

	public void remove() {
		remove(0);
	}
    
    
    /**
     * Saves the state of this {@code UnrolledLinkedList} instance to a stream
     * (that is, serializes it).
     *
     * @serialData The size of the list (the number of elements it
     *             contains) is emitted (int), followed by all of its
     *             elements (each an Object) in the proper order.
     */
    private void writeObject(java.io.ObjectOutputStream s)
        throws java.io.IOException {

        // Write out size
        s.writeInt(m_size);

        // Write out all elements in the proper order.
        for(Node<E> node = m_firstNode; node != null; node = node.next)
        	s.writeObject(node);
    }

    /**
     * Reconstitutes this {@code UnrolledLinkedList} instance from a stream
     * (that is, deserializes it).
     */
    @SuppressWarnings("unchecked")
    private void readObject(java.io.ObjectInputStream s)
        throws java.io.IOException, ClassNotFoundException {

        // Read in size
        int size = s.readInt();

        Node<E> node;
        // Read in all elements in the proper order.
        for (int i = 0; i < size; ++i) {
            node = (Node<E>) s.readObject();
        	if(m_firstNode == null)
            	m_firstNode = m_lastNode = node;
        	else {
        		m_lastNode.next = node;
        		node.prev = m_lastNode;
        		m_lastNode = node; 
        	}
        	i += node.size;
        }
        
    }
    
    
    /**
	 * @author Furkan Tektas<tektasfurkan@gmail.com>
	 * @version 1.0
	 * @updated 23-Mar-2013 14:56:15
	 * 
	 * Holds the data for the UnrolledLinkedList as an array. Default array capacity
	 * can be set by constructor arguments.
	 */
	private static class Node<E> implements Serializable {
		

		// Next node.
		Node<E> next;

		// Previous node.
		Node<E> prev;

		// Number of elements stored in current node.
		public int size = 0;
		public int capacity;

		// Element container
		E[] elements;

		/**
		 * Returns whether the node is full or not. 
		 * @return true if the node is full.
		 */
		public boolean isFull() {
			return (size == capacity);
		}

		@SuppressWarnings("unchecked")
		public Node(int capacity) {
			this.capacity = capacity;
			elements = (E[]) new Object[capacity];
		}
		
		@SuppressWarnings("unused")
		public Node() {
			this(m_DefaultNodeCapacity);
		}

		private static final long serialVersionUID = 4774210057368446882L;
		
	    /**
	     * Saves the state of this instance to a stream
	     * (that is, serializes it).
	     *
	     * @serialData The size of the node (the number of elements it
	     *             contains) is emitted (int), followed by all of its
	     *             elements (each an Object) in the proper order.
	     */
	    private void writeObject(java.io.ObjectOutputStream s)
	        throws java.io.IOException {
	        // Write out size
	        s.writeInt(size);

	        int i = 0;
	        // Write out all elements in the proper order.
	        for (E x = elements[i]; i < size ; x = elements[i], ++i)
	            s.writeObject(x);
	    }

	    /**
	     * Reconstitutes this instance from a stream
	     * (that is, deserializes it).
	     */
	    @SuppressWarnings("unchecked")
	    private void readObject(java.io.ObjectInputStream s)
	        throws java.io.IOException, ClassNotFoundException {
	        // Read in size
	        size = s.readInt();
	        capacity = size + 2;
	        
			elements = (E[]) new Object[capacity];

	        // Read in all elements in the proper order.
	        for (int i = 0; i < size; i++)
	            elements[i] = ((E)s.readObject());
	    }

	}// end Node
    
    /**
     * Returns a list-iterator of the elements in this list (in proper
     * sequence), starting at the specified position in the list.
     * Obeys the general contract of {@code List.listIterator(int)}.<p>
     *
     * @param index index of the first element to be returned from the
     *              list-iterator (by a call to {@code next})
     * @return a ListIterator of the elements in this list (in proper
     *         sequence), starting at the specified position in the list
     * @throws IndexOutOfBoundsException {@inheritDoc}
     * @see List#listIterator(int)
     */
	public class UListIterator implements ListIterator<E> {

		private Node<E> currentNode;
		private int index;
		private int nodePosition;
		private int lastReturnedPosition = -1;
				
		public UListIterator(int index) {
			this.index = index;
			
			int i;
			if(index < (m_size >> 1)) {
				i = 0;
				currentNode = m_firstNode;
				while((i += currentNode.size) < index)
					currentNode = currentNode.next;
				
				nodePosition = index - i +currentNode.size;
			} else {
				i = m_size;
				currentNode = m_lastNode;
				while((i -= currentNode.size) > index)
					currentNode = currentNode.prev;
				
				nodePosition = index - i;
			} 
		}
		
		public UListIterator() {
			this(0);
		}
		
		@Override
		public void add(E e) {
			Node<E> nodeToInsert = currentNode;
			
			if(currentNode.isFull()) {			
				int movingElementCount = 2; //currentNode.size/2
				int initialIndex = currentNode.size - movingElementCount;
				
				Node<E> nextNode = currentNode.next;
				
				// new node capacity = (currentNOde.capacity + currentNode.size)/2
				Node<E> newNode = new Node<E>(currentNode.capacity);
				
				currentNode.next = newNode;
				if(nextNode != null) 
					nextNode.prev = newNode;

				newNode.prev = currentNode;
				newNode.next = nextNode;
				
				

				//balancing the elements between current and new node
				System.arraycopy(currentNode.elements, initialIndex, newNode.elements, 0, movingElementCount);

				
				newNode.size += movingElementCount;
				currentNode.size -= movingElementCount;
				
				// if the current node is the last node,
				// then update the last node
				if(currentNode == m_lastNode)
					m_lastNode = newNode;
				 
				// if nodePosition-th element is in the new node
				// change current node to next node and update the
				// nodePosition
				if(nodePosition > currentNode.size) {
					nodePosition -= currentNode.size;
					currentNode = nodeToInsert = currentNode.next;
				}
			}
						
			//shifting the elements after the nodePosition
			if(nodePosition < nodeToInsert.size)
				System.arraycopy(nodeToInsert.elements, nodePosition, nodeToInsert.elements, nodePosition+1, nodeToInsert.size-nodePosition);

			
			nodeToInsert.elements[nodePosition] = e;
			
			lastReturnedPosition = -1;
			++nodePosition;
			++nodeToInsert.size;
			++m_size;
		}

		@Override
		public boolean hasNext() {
			return (index < m_size );
		}

		@Override
		public boolean hasPrevious() {
			return (index > 0);
		}

		@Override
		public E next() {
			
			if(nodePosition >= currentNode.size) {
				if(currentNode.next != null) {
					currentNode = currentNode.next;
					nodePosition = 0;
				} else {
					throw new NoSuchElementException();
				}
			}
			++index;
			
			lastReturnedPosition = nodePosition;
			return currentNode.elements[nodePosition++];
		}

		@Override
		public int nextIndex() {
			return index;
		}

		@Override
		public E previous() {
			if(nodePosition <= 0) {
				if(currentNode.prev != null) {
					currentNode = currentNode.prev;
					nodePosition = currentNode.size;
				} else {
					throw new NoSuchElementException();
				}
			}
			--index;
			
			lastReturnedPosition = --nodePosition;
			return currentNode.elements[nodePosition];
		}

		@Override
		public int previousIndex() {
			return (index - 1);
		}

		@Override
		public void remove() {
			for(int i = lastReturnedPosition; i < currentNode.size-1; ++i)
				currentNode.elements[i] = currentNode.elements[i+1];
			currentNode.elements[--currentNode.size] = null;
			--m_size;

			// node is empty, removing  the node
			if(currentNode.size == 0 && currentNode.prev != null && currentNode.next != null) {
				currentNode.prev.next = currentNode.next;
				currentNode.next.prev = currentNode.prev;
				currentNode = currentNode.prev;	
			}
			lastReturnedPosition = -1;
		}

		@Override
		public void set(E e) {
			 if (lastReturnedPosition == -1)
	                throw new IllegalStateException();
			
			currentNode.elements[lastReturnedPosition] = e;
		}
		
	}
}// end UnrolledLinkedList