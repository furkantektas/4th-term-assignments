package com.furkantektas.assignments.cse222.hw03;

import java.util.LinkedList;
import java.util.Scanner;

public class RegressionTest {

	public static void main(String[] args) {
		UnrolledLinkedList<Integer> ull = new UnrolledLinkedList<Integer>(20);
		LinkedList<Integer> ll = new LinkedList<Integer>();
		Scanner sc = new Scanner(System.in);
		sc.next();
		sc.close();
		System.out.println("##### TEST WITH INTEGER #####\n\n");
		testULL(ull);
		
		System.out.println("\n\n\n");
		testLL(ll);
	}
	
	public static void testULL(UnrolledLinkedList<Integer> ll) {	
		System.out.println("Unrolled Linked List");
		System.out.println("---------------------------");
		
		
		long time = System.currentTimeMillis();
		for(Integer i = 0; i < 1000000; ++i)
			ll.add(i);
		System.out.println("1.000.000 add() => " + (System.currentTimeMillis() - time) + "msec");

		time = System.currentTimeMillis();
		for(Integer i = 0; i < 1000000; ++i) 
			ll.remove();
		
		System.out.println("1.000.000 remove() => " + (System.currentTimeMillis() - time) + "msec");
	}
	
	public static void testLL(LinkedList<Integer> ll) {	
		System.out.println("Linked List");
		System.out.println("---------------------------");
		
		
		long time = System.currentTimeMillis();
		for(Integer i = 0; i < 1000000; ++i)
			ll.add(i);
		System.out.println("1.000.000 add() => " + (System.currentTimeMillis() - time) + "msec");

		
		time = System.currentTimeMillis();
		for(Integer i = 0; i < 1000000; ++i)
			ll.remove();
		System.out.println("1.000.000 remove() => " + (System.currentTimeMillis() - time) + "msec");
	}

}
