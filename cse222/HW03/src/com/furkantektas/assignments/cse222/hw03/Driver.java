/*
 * File:    Driver.java
 *
 * Course:  CSE222
 * Project: HW03
 * Created: 23.03.2013 14:46:12
 * 
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */
package com.furkantektas.assignments.cse222.hw03;

import java.util.LinkedList;

public class Driver {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		UnrolledLinkedList<String> ull = new UnrolledLinkedList<String>(20);
		LinkedList<String> ll = new LinkedList<String>();
		
		testULL(ull);
		System.out.println("\n\n\n");
		testLL(ll);
	}
	
	public static void testULL(UnrolledLinkedList<String> ll) {	
		System.out.println("Unrolled Linked List");
		System.out.println("---------------------------");
		System.out.println("Adding integers [0-9]");
		
		for(int i = 0; i < 10; ++i)
			ll.add(String.valueOf(i));
		System.out.println(ll);

		System.out.println("Removing  the 5th indexed element");
		ll.remove(5);
		System.out.println(ll);
		
		System.out.println("Adding 5 back:");
		ll.add(5,"5");
		System.out.println(ll);
		
		System.out.println("getFirst() => " + ll.getFirst());
		System.out.println("getLast() => " + ll.getLast());
		System.out.println("get(3) => " + ll.get(3));

		System.out.println("\n");
		System.out.println("iterator().next() => " + ll.iterator().next());
		System.out.println("listIterator().next() => " + ll.listIterator().next());
		System.out.println("listIterator(3).next() => " + ll.listIterator(3).next());
		
	}
	
	public static void testLL(LinkedList<String> ll) {	
		System.out.println("Linked List");
		System.out.println("---------------------------");
		System.out.println("Adding integers [0-9]");
		
		for(int i = 0; i < 10; ++i)
			ll.add(String.valueOf(i));
		System.out.println(ll);

		System.out.println("Removing  the 5th indexed element");
		ll.remove(5);
		System.out.println(ll);
		
		System.out.println("Adding 5 back:");
		ll.add(5,"5");
		System.out.println(ll);
		
		System.out.println("getFirst() => " + ll.getFirst());
		System.out.println("getLast() => " + ll.getLast());
		System.out.println("get(3) => " + ll.get(3));

		System.out.println("\n");
		System.out.println("iterator().next() => " + ll.iterator().next());
		System.out.println("listIterator().next() => " + ll.listIterator().next());
		System.out.println("listIterator(3).next() => " + ll.listIterator(3).next());
	}
}