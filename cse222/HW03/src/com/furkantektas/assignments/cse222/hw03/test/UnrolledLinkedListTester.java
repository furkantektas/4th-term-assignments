/*
 * File:    UnrolledLinkedListTester.java
 *
 * Course:  CSE222
 * Project: HW03
 * Created: 02.04.2013 14:46:12
 * 
 * Author:  Furkan Tektas<tektasfurkan@gmail.com>
 * Number:  111044029
 *
 */
package com.furkantektas.assignments.cse222.hw03.test;

import static org.junit.Assert.*;

import java.util.ListIterator;

import org.junit.Test;

import com.furkantektas.assignments.cse222.hw03.UnrolledLinkedList;

/**
 * @author Furkan Tektas<tektasfurkan@gmail.com>
 *
 */
public class UnrolledLinkedListTester {
	
	@Test
	public void testListIterator() {
		UnrolledLinkedList<Integer> ull = new UnrolledLinkedList<Integer>();
		
		Integer a = new Integer(0);
		ull.listIterator().add(a);
		assertEquals(a, ull.listIterator(0).next());
	}
	
	@Test
	public void testSet() {
		UnrolledLinkedList<Integer> ull = new UnrolledLinkedList<Integer>();
		
		Integer a = new Integer(0);
		Integer b = new Integer(1);
		ull.addFirst(a);
		ull.set(0,b);
		
		assertEquals(b, ull.getFirst());
	}

	@Test
	public void testGet() {
		UnrolledLinkedList<Integer> ull = new UnrolledLinkedList<Integer>();
		
		Integer a = new Integer(0);
		ull.addFirst(a);
		assertEquals(a, ull.get(0));
	}
	
	@Test
	public void testGetFirst() {
		UnrolledLinkedList<Integer> ull = new UnrolledLinkedList<Integer>();
		
		Integer a = new Integer(0);
		ull.addFirst(a);
		assertEquals(a, ull.getFirst());
	}
	
	@Test
	public void testAdd() {
		UnrolledLinkedList<Integer> ull = new UnrolledLinkedList<Integer>();
		
		Integer a = new Integer(0);
		ull.add(0, a);
		assertEquals(a, ull.get(0));
	}
	
	@Test
	public void testAddFirst() {
		UnrolledLinkedList<Integer> ull = new UnrolledLinkedList<Integer>();
		
		Integer a = new Integer(0);
		ull.addFirst(a);
		assertEquals(a, ull.get(0));
	}
	
	@Test
	public void testGetLast() {
		UnrolledLinkedList<Integer> ull = new UnrolledLinkedList<Integer>();
		
		Integer a = new Integer(0);
		ull.addLast(a);
		assertEquals(a, ull.getLast());
	}
	
	@Test
	public void testAddLast() {
		UnrolledLinkedList<Integer> ull = new UnrolledLinkedList<Integer>();
		
		Integer a = new Integer(0);
		ull.addLast(a);
		assertEquals(a, ull.getLast());
	}
	
	@Test
	public void testListIteratorNext() {
		UnrolledLinkedList<Integer> ull = new UnrolledLinkedList<Integer>();
		
		Integer a = new Integer(0);
		ull.addFirst(a);
		
		assertEquals(a, ull.listIterator().next());
	}
	
	@Test
	public void testListIteratorPrev() {
		UnrolledLinkedList<Integer> ull = new UnrolledLinkedList<Integer>();
		
		Integer a = new Integer(0);
		Integer aNext = new Integer(1);
		ull.addFirst(aNext);
		ull.addFirst(a);
		
		assertEquals(a, ull.listIterator(1).previous());
	}
	
	@Test
	public void testListIteratorNextIndex() {
		UnrolledLinkedList<Integer> ull = new UnrolledLinkedList<Integer>();
		
		Integer a = new Integer(0);
		ull.addFirst(a);
		
		assertEquals(0, ull.listIterator().nextIndex());
	}
	
	@Test
	public void testListIteratorAdd() {
		UnrolledLinkedList<Integer> ull = new UnrolledLinkedList<Integer>();
		
		Integer a = new Integer(0);
		ull.listIterator().add(a);
		
		assertEquals(a, ull.listIterator().next());
	}
	
	@Test
	public void testListIteratorRemove() {
		UnrolledLinkedList<Integer> ull = new UnrolledLinkedList<Integer>();
		
		Integer a = new Integer(0);
		ull.addFirst(a);
		ListIterator<Integer> lI = ull.listIterator();
		
		lI.next();
		lI.remove();
		
		assertEquals(0, ull.size());
	}
	
	@Test
	public void testListIteratorSet() {
		UnrolledLinkedList<Integer> ull = new UnrolledLinkedList<Integer>();
		
		Integer a = new Integer(0);
		Integer b = new Integer(1);
		ull.addFirst(a);
		ListIterator<Integer> lI = ull.listIterator();
		
		lI.next();
		lI.set(b);
		
		assertEquals(b, ull.getFirst());
	}
	
	
	
}
