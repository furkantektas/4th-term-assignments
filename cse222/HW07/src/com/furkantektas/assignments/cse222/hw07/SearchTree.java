package com.furkantektas.assignments.cse222.hw07;

public interface SearchTree<E> {
	/**
	 * Inserts <code>item</code> where it belongs in the tree. Returns 
	 * <code>true</code> if item is inserted; <code>false</code> if it 
	 * isn't (already in tree).
	 * @param item Item to be inserted
	 * @return <code>true</code> if item is inserted <code>false</code> 
	 * if it isn't.
	 */
	public boolean add(E item);
	
	/**
	 * Returns true if <code>target</code> is found in the tree.
	 * @param item item to be checked.
	 * @return <code>true</code> if tree contains target, <code>false</code> 
	 * if it isn't. 
	 */
	public boolean contains(E target);
	
	/**
	 * Returns a reference to the node with <code>target</code> as its data 
	 * if target is found. Otherwise, returns <code>null</code>.
	 * @param target
	 * @return target's data if target is found in tree, otherwise null.
	 */
	public E find(E target);
	
	/**
	 * Removes <code>target</code> (if found) from tree and returns it; 
	 * otherwise, returns <code>null</code>.
	 * @param target item to be deleted.
	 * @return deleted item if it's found, otherwise null.
	 */
	public E delete(E target);
	
	/**
	 * Removes the <code>target</code>(if found) from tree and returns 
	 * <code>true</code>; otherwise, returns <code>false</code>.
	 * @param target
	 * @return
	 */
	public boolean remove(E target);
}
