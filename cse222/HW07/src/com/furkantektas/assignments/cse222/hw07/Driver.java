package com.furkantektas.assignments.cse222.hw07;

public class Driver {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Integer Test:");
		BST<Integer> bst = new BST<Integer>();
		bst.add(8);
		bst.add(7);
		bst.add(9);
		bst.add(3);
		bst.add(1);
		bst.add(5);
		bst.add(4);
		bst.add(6);
		
		System.out.println(bst);
		
		System.out.println("String Test:");
		BST<String> bstS = new BST<String>();
		bstS.add("aba");
		bstS.add("caba");
		bstS.add("aaa");
		bstS.add("aac");
		bstS.add("daac");
		
		
		String str = new String("searchTerm");
		bstS.add(str);
		System.out.println(bstS);
	}

}
