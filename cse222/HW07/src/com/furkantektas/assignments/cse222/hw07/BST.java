package com.furkantektas.assignments.cse222.hw07;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * BST is an ArrayList based binary search tree implementation. 
 * 
 * @author Furkan Tektas<tektasfurkan@gmail.com>
 * Student Number : 111044029
 *
 * @param <E> - Any class. If no appropriate comparator is given, then class
 * should be Comparable. 
 */
public class BST<E> implements Cloneable, Serializable, BinaryTree<E>,
		SearchTree<E> {

	private static final long serialVersionUID = 414277996910013563L;

	private int size = 0;

	private List<E> data = new ArrayList<E>();

	private final Comparator<? super E> comparator;

	public BST() {
		this(null);
	}

	public BST(Comparator<? super E> comparator) {
		this.comparator = comparator;
	}

	@Override
	public boolean add(E item) {
		return add(item, 0);
	}

	private boolean add(E item, int index) {

		if (isEmpty()) {
			data.add(item);
			++size;
			return true;
		}

		if (contains(item))
			return false;

		int compareResult;
		while (rangeCheck(index)) {

			if (!rangeCheck(index) || data.get(index) == null)
				break;

			compareResult = compare(data.get(index), item);

			if (compareResult < 0) // check left child
				index = getLeftChildIndex(index);
			else if (compareResult > 0) // check right child
				index = getRightChildIndex(index);
		}

		resize(index);
		data.set(index, item); // check size
		++size;
		return true;
	}

	@Override
	public boolean contains(E target) {
		return (find(target) != null);
	}

	@Override
	public E find(E target) {
		int index = findIndex(target);

		if(rangeCheck(index))
			return data.get(index);
		return null;
	}

	/**
	 * Returns the index of the target if its found. Otherwise returns -1.
	 * 
	 * @param target
	 * @return
	 */
	private int findIndex(E target) {
		int index = 0;
		int compareResult;

		if (target == null)
			return -1;

		while (rangeCheck(index)) {
			if (data.get(index) == null)
				return -1;

			compareResult = compare(data.get(index), target);

			if (compareResult == 0)
				return index;
			else if (compareResult < 0) // check left child
				index = getLeftChildIndex(index);
			else if (compareResult > 0) // check right child
				index = getRightChildIndex(index);
		}

		if (data.size() <= index)
			return -1;
		return index;
	}

	/**
	 * Returns the biggest element of subtree beginning of index.
	 * 
	 * @param index
	 * @return
	 */
	private E findMax(int index) {
		int maxIndex;

		if (rangeCheck(getLeftChildIndex(index))
				&& data.get(getLeftChildIndex(index)) != null)
			maxIndex = findMaxIndex(getLeftChildIndex(index));
		else
			maxIndex = findMaxIndex(getRightChildIndex(index));

		if (rangeCheck(maxIndex))
			return data.get(maxIndex);
		return null;
	}

	/**
	 * Returns the biggest element's index of subtree beginning of index.
	 * 
	 * @param index
	 * @return
	 */
	private int findMaxIndex(int index) {
		int biggestChildIndex = index;

		if (data.size() <= index || data.get(index) == null)
			return index;

		biggestChildIndex = findMaxIndex(getRightChildIndex(biggestChildIndex));

		if (data.size() <= biggestChildIndex
				|| data.get(biggestChildIndex) == null
				|| (compare(data.get(index), data.get(biggestChildIndex)) <= 0))
			return index;
		return biggestChildIndex;
	}

	/**
	 * Returns the smallest element of subtree beginning of index.
	 * 
	 * @param index
	 * @return
	 */
	@SuppressWarnings("unused")
	private E findMin(int index) {
		int minIndex;
		if (rangeCheck(getRightChildIndex(index))
				&& data.get(getRightChildIndex(index)) != null)
			minIndex = findMinIndex(getRightChildIndex(index));
		else
			minIndex = findMinIndex(getLeftChildIndex(index));

		if (rangeCheck(minIndex))
			return data.get(minIndex);
		return null;
	}

	/**
	 * Returns the smallest element's index of subtree beginning of index.
	 * 
	 * @param index
	 * @return
	 */
	private int findMinIndex(int index) {
		int smallestChildIndex = index;

		if (data.size() <= index || data.get(index) == null)
			return index;

		smallestChildIndex = findMinIndex(getLeftChildIndex(smallestChildIndex));

		if (data.size() <= smallestChildIndex
				|| data.get(smallestChildIndex) == null
				|| (compare(data.get(index), data.get(smallestChildIndex)) >= 0))
			return index;
		return smallestChildIndex;
	}

	@Override
	public E delete(E target) {
		if (deleteRecursive(target) != null) {
			--size;
			return target;
		}
		return null;
	}

	/**
	 * Deletes and regenerates the new structure of the tree.
	 * 
	 * @param target
	 *            - element to be removed.
	 * @return
	 */
	private E deleteRecursive(E target) {
		int index = findIndex(target), rIndex = getRightChildIndex(index), lIndex = getLeftChildIndex(index);

		if (rangeCheck(index)) {
			if (isLeaf(index))
				data.set(index, null);
			else if (!isFilled(lIndex)) {
				shiftUpTree(rIndex);
			} else {
				data.set(index, deleteRecursive(findMax(index)));
				return target;
			}
		}
		return target;
	}

	/**
	 * Shifts the tree started with index up one level.
	 * 
	 * @param index
	 *            - Initial index of the subtree.
	 */
	private void shiftUpTree(int index) {
		int parentIndex = getParentIndex(index);
		if (rangeCheck(index) && rangeCheck(parentIndex)) {
			data.set(parentIndex, data.get(index));

			if (rangeCheck(getLeftChildIndex(index))) {
				data.set(getLeftChildIndex(parentIndex),
						data.get(getLeftChildIndex(index)));
			} else {
				data.set(index, null);
			}

			if (rangeCheck(getRightChildIndex(index))) {
				data.set(getRightChildIndex(parentIndex),
						data.get(getRightChildIndex(index)));
			} else {
				data.set(index, null);
			}

			if (isFilled(getLeftChildIndex(index)))
				shiftUpTree(getLeftChildIndex(index));

			shiftUpTree(getRightChildIndex(index));
		}
	}

	@Override
	public boolean remove(E target) {
		return (delete(target) != null);
	}

	@Override
	public BinaryTree<E> getLeftSubtree() {
		return null;
	}

	@Override
	public BinaryTree<E> getRightSubtree() {
		return null;
	}

	@Override
	public E getData() {
		if (!data.isEmpty())
			return data.get(0);
		return null;
	}

	@Override
	public boolean isLeaf() {
		return (size() == 1);
	}

	/**
	 * Returns true if the index-th element has no left and right subtrees.
	 * 
	 * @param index
	 * @return
	 */
	private boolean isLeaf(int index) {
		int rIndex = getRightChildIndex(index), lIndex = getLeftChildIndex(index);

		if (rIndex >= data.size() && lIndex >= data.size())
			return true;
		if (rangeCheck(rIndex) && rangeCheck(lIndex))
			return (data.get(lIndex) == null && data.get(rIndex) == null);
		return false;
	}

	/**
	 * Returns true if the tree is empty.
	 */
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public void clear() {
		data.clear();
		size = 0;
	}

	@Override
	public int size() {
		return size;
	}

	public int depth() {
		return depth(0);
	}
	
	private int depth(int index) {
		int lDepth,
		    rDepth;
		
		if(!rangeCheck(index))
			return 0;
		
		lDepth = depth(getLeftChildIndex(index));
		rDepth = depth(getRightChildIndex(index));
		
		return 1 + Math.max(lDepth, rDepth);
	}
	
	
	/**
	 * Expands the list by adding dummy null values until list size reaches the
	 * newSize.
	 * 
	 * @param newSize
	 *            - list's new size
	 */
	private void resize(int newSize) {
		for (int i = data.size(); i <= newSize; ++i)
			data.add(null);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		print(sb, 0);
		return sb.toString();
	}

	/**
	 * Wrapper method of print.
	 * 
	 * @param sb
	 * @param index
	 */
	private void print(StringBuilder sb, int index) {
		print(sb, "", index);
	}

	/**
	 * Appends the structure of the tree to the sb.
	 * 
	 * @param sb
	 *            - StringBuilder to tree structure be appended.
	 * @param prefix
	 * @param index
	 */
	private void print(StringBuilder sb, String prefix, int index) {
		if (data.size() <= index)
			return;
		sb.append(data.get(index) + "\n");
		prefix = prefix + "|   ";
		if (data.get(index) != null) {
			if (rangeCheck(getLeftChildIndex(index))) {
				sb.append(prefix);
				sb.append("|---");
				print(sb, prefix, getLeftChildIndex(index));
			}

			if (data.size() > getRightChildIndex(index)) {
				sb.append(prefix);
				sb.append("----");
				print(sb, prefix, getRightChildIndex(index));
			}
		}

	}

	/**
	 * Compares item1 with item2 and returns the comparison result. Uses
	 * comparator if it's not null, uses item1's compareTo() method otherwise.
	 * 
	 * @param item1
	 * @param item2
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private int compare(E item1, E item2) {
		if (comparator != null)
			return comparator.compare(item1, item2);
		return ((Comparable<? super E>) item2).compareTo(item1);
	}

	/**
	 * Validates the index.
	 * 
	 * @param index
	 * @return
	 */
	private boolean rangeCheck(int index) {
		return (index >= 0 && index < data.size());
	}

	/**
	 * Returns true if the index-th cell is in the range of data and is not null
	 * 
	 * @param index
	 * @return
	 */
	private boolean isFilled(int index) {
		return (rangeCheck(index) && data.get(index) != null);
	}
	
	private int getParentIndex(int childIndex) {
		// right child
		if ((childIndex % 2) == 0)
			return (childIndex / 2 - 1);
		// left child
		return ((childIndex + 1) / 2 - 1);
	}

	private int getRightChildIndex(int index) {
		return (index + 1) * 2;
	}

	private int getLeftChildIndex(int index) {
		return (index + 1) * 2 - 1;
	}
}
