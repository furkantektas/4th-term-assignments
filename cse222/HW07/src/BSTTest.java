import static org.junit.Assert.*;

import org.junit.Test;

import com.furkantektas.assignments.cse222.hw07.BST;


public class BSTTest {

	
	@Test
	public void rootDeleteTestWithTwoChildNode() {
		BST<Integer> bst = new BST<Integer>();
		bst.add(8);
		bst.add(7);
		bst.add(9);
		bst.add(3);
		bst.add(1);
		bst.add(5);
		bst.add(4);
		bst.add(6);
		
		bst.delete(8);
		
		assertNotNull(bst.getData());
		assertEquals(new Integer(7), bst.getData());
	}
	
	@Test
	public void rootDeleteTestOnlyLeftNode() {
		BST<Integer> bst = new BST<Integer>();
		bst.add(8);
		bst.add(7);
		bst.add(6);
		bst.add(5);
		
		bst.delete(8);
		
		assertNotNull(bst.getData());
		assertEquals(new Integer(7), bst.getData());
	}
	
	@Test
	public void rootDeleteTestOnlyRightNode() {
		BST<Integer> bst = new BST<Integer>();
		bst.add(5);
		bst.add(6);
		bst.add(7);
		bst.add(8);
		
		bst.delete(5);
		
		assertNotNull(bst.getData());
		assertEquals(new Integer(6), bst.getData());
	}
	
	@Test
	public void rootDeleteTestWithoutNode() {
		BST<Integer> bst = new BST<Integer>();
		bst.delete(5);
		
		assertEquals(null, bst.getData());
	}
	
	@Test
	public void testSize() {
		BST<Integer> bst = new BST<Integer>();
		bst.add(5);
		bst.add(6);
		bst.add(7);
		
		int oldSize = bst.size();
		bst.delete(5);
		assertTrue(oldSize == 3);
		assertEquals(oldSize, bst.size()+1);
	}
	

	@Test
	public void testClear() {
		BST<Integer> bst = new BST<Integer>();
		bst.add(5);
		bst.add(6);
		bst.add(7);
		
		bst.clear();
		assertEquals(0, bst.size());
	}
	
	@Test
	public void testDepth() {
		BST<Integer> bst = new BST<Integer>();
		bst.add(5);
		bst.add(6);
		bst.add(7);
		
		assertEquals(3, bst.depth());
		bst.clear();
		
		bst.add(8);
		bst.add(7);
		bst.add(6);
		bst.add(5);
		
		assertEquals(4, bst.depth());
	}
	
	@Test
	public void testFind() {
		BST<Integer> bst = new BST<Integer>();
		bst.add(5);
		bst.add(6);
		bst.add(7);
		
		Integer num = new Integer(9);
		bst.add(num);
		
		assertEquals(num, bst.find(num));
	}
	
	@Test
	public void testFindWithStrings() {
		BST<String> bst = new BST<String>();
		bst.add("aba");
		bst.add("caba");
		bst.add("aaa");
		bst.add("aac");
		bst.add("daac");
		
		
		String str = new String("searchTerm");
		bst.add(str);
		
		assertEquals(str, bst.find(str));
	}
	
	
	
	
	
	
	
	

}
