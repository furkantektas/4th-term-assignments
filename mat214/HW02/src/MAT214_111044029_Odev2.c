/*
 ============================================================================
 Name        : HW02.c
 Author      : Furkan Tektas<tektasfurkan@gma
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#ifndef NULL
	#define NULL '\0'
#endif

typedef struct {
	int width;
	int height;
	float** mx;

} matrix;

/**
 * Allocates a memory for matrix[width x height]
 */
matrix* mallocForMatrix(int width, int height);

/**
 * Frees the allocated memory for matrix.
 */
void freeMatrix(matrix* mx);

/**
 * Prints matrix mx
 */
void printMatrix(matrix* mx);

/**
 * Returns the minor matrix ( Original matrix without
 * the column i and row j)
 */
matrix* getMinorMatrix(matrix* mx, int i, int j);

/**
 * Returns 1 if the matrix's width and height are equal to 2
 */
int is2x2(matrix* mx);

/**
 * Returns the determinant of 2x2 matrix
 */
float det2x2(matrix* mx);

/**
 *
 * Returns the determinant of nxn matrix.
 * This function may exit the program with appropriate error messages.
 * Pre: mx->width == mx->height
 *      userInput num is equal to width * height
 */
float det(matrix *mx);

/**
 * Prints the basic usage of program.
 */
void showUsage(void);

/**
 * Creates a [width * height] matrix with elements(userInput)
 * userInput should consist width * height elements and all elements
 * should be seperated by comma.
 * (Eg: createMatrix(2,2,"1,2,3,4"); )
 */
matrix* createMatrix(int width, int height, char* userInput);

int main(int argc, char** argv) {
	matrix *mx;

	/* Illegal number of arguments, print usage */
	if(argc != 4) {
		showUsage();
		return 1;
	}

	/* Creating matrix from command line arguments */
	mx = createMatrix(atoi(argv[1]), atoi(argv[2]), argv[3]);


	/* Printing the matrix and its determinant in appropriate style */
	printf("\n=========== MATRIX ==========");
	printf("\n=============================\n");
	printMatrix(mx);
	printf("=============================\n\n");
	printf("Determinant : %5.2f\n\n", det(mx));
	return 0;
}


matrix* mallocForMatrix(int width, int height) {
	int i;
	matrix* mx = (matrix *) malloc(sizeof(matrix));
	mx->width = width;
	mx->height = height;

	mx->mx = (float**) (malloc(sizeof(float *)*mx->height));

	for(i = 0; i < mx->height; ++ i)
		mx->mx[i] = (float *) malloc(sizeof(float)*mx->width);

	return mx;
}

void freeMatrix(matrix* mx) {
	int i;
	for(i=0; i< mx->height; ++i)
		free(mx->mx[i]);
	free(mx->mx);
	free(mx);
}

void printMatrix(matrix* mx) {
	int i,j;
	for(i = 0; i < mx->height; ++i) {
		for(j = 0; j < mx->width; ++j)
			printf("%8.2f ", mx->mx[i][j]);
		printf("\n");
	}
}

matrix* getMinorMatrix(matrix* mx, int i, int j) {
	int minorIIndex, minorJIndex, mxIIndex, mxJIndex;
	matrix* minor = mallocForMatrix(mx->width-1, mx->height-1);

	for(minorIIndex = 0; minorIIndex < mx->height-1; ++minorIIndex) {
		mxIIndex = (minorIIndex >= i) ? (minorIIndex + 1) : minorIIndex;

		for(minorJIndex = 0; minorJIndex < mx->width-1; ++minorJIndex) {
			mxJIndex = (minorJIndex >= j) ? (minorJIndex + 1) : minorJIndex;

			minor->mx[minorIIndex][minorJIndex] = mx->mx[mxIIndex][mxJIndex];
		}
	}

	return minor;
}

int is2x2(matrix* mx) {
	return ((mx->width == 2) && (mx->height  == 2));
}

float det2x2(matrix* mx) {
	return (mx->mx[0][0] * mx->mx[1][1] - mx->mx[0][1] * mx->mx[1][0]);
}

float det(matrix *mx) {
	matrix* minor;
	int j;
	float determinant = 0.0,
	      coeff;

	if(is2x2(mx))
		return det2x2(mx);


	for(j=0; j < mx->height; ++j) {
		coeff = mx->mx[0][j] * pow(-1.0,j);
		minor = getMinorMatrix(mx,0,j);
		determinant += coeff * det(minor);
		freeMatrix(minor);
	}

	return determinant;
}

void showUsage(void) {
	printf("Usage: [PROGRAM NAME] [WIDTH] [HEIGHT] [ELEMENTS SEPERATED BY COMMA]\n");
}

matrix* createMatrix(int width, int height, char* userInput) {
	matrix *mx;
	int i=0,j=0;
	char *buffer;

	if(width != height) {
		printf("It's not a square matrix.\n");
		showUsage();
		exit(-1);
	}

	mx = mallocForMatrix(width,height);
	mx->width = width;
	mx->height = height;

	buffer = strtok(userInput,",");
	while(buffer != NULL) {
		mx->mx[i][j] = atof(buffer);

		buffer = strtok(NULL, ",");

		if(buffer != NULL) {
			++j;
			if(!(j%width)) {
				++i;
				j = 0;
				if(i >= height) {
					printf("Invalid arguments.\n");
					showUsage();
					printf("%d %d\n", i,j);
					exit(-1);
				}
			}
		}
	}
	return mx;
}
