/*
 ============================================================================
 Name        : hw1.c
 Author      : Furkan Tektas<tektasfurkan@gmail.com>
 Version     :
 Copyright   : Mat214 - HW01
 Description : Calculates the root via bisection, false position,
               secant, Newton-Raphson methods.
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define ROOTTOLERANCE 0.1

// root will be searched between a-b
// which b-a = 1000

#define INTERVAL 3

// a and b will be pushed by PUSH
#define PUSH 3

double a, // left value
       b, //right value
       funcA, // f(a)
       funcB; // f(b)

double func(double x);

/**
 * Derivative of function.
 */
double defFunc(double x);

/**
 * Returns 1 if num > 0
 */
int isPositive(double num);

/**
 * Returns 1 if num < 0
 */
int isNegative(double num);

/**
 * Decreases the a by INTERVAL/2 and
 * pushed right by PUSH
 */
double decreaseA();

/*
 * Increases the a by INTERVAL/2 and
 * pushed right by PUSH
 */
double increaseB();

/**
 * Find the root of the equation iteratively
 * by bisection method.
 */
double findViaBisection(double lVal, double rVal);

/**
 * Find the root of the equation iteratively
 * by secant method.
 */
double findViaSecant(double lVal, double rVal);

/**
 * Find the root of the equation iteratively
 * by false position method.
 */
double findViaFalsePosition(double lVal, double rVal);

/**
 * Find the root of the equation iteratively
 * by Newton-Raphson method.
 */
double findViaNewtonRaphson(double val);


int main(void) {
	a = 0;
	b = 0;
	a = decreaseA();
	b = increaseB();

	printf("Result via bisection = %.5f\n", findViaBisection(a,b));

	a = 0;
	b = 0;
	a = decreaseA();
	b = increaseB();
	printf("Result via secant = %.5f\n", findViaSecant(a,b));

	a = 0;
	b = 0;
	a = decreaseA();
	b = increaseB();
	printf("Result via false position = %.5f\n", findViaFalsePosition(a,b));

	a = 0;
	b = 0;
	a = decreaseA();
	b = increaseB();
	printf("Result via Norton-Raphson = %.5f\n", findViaNewtonRaphson(a));

	return EXIT_SUCCESS;
}


//(-3000*1.2^x + 175*x)/(1.2^x-1) + 5000
//(−3000×1.2^5+175×5)÷(1.2^5−1)+5000
double func(double x) {
	double commonPow = pow(1.2,x);
	return (
			(-3000*commonPow + 175*x)/(commonPow-1) + 5000
			);
}

double defFunc(double x) {
	double commonPow = pow(1.2,x);
	return -(175*( commonPow*(0.182322*x-4.12551) + 6.49639*pow(10,-16)*pow(M_E,(0.364643*x))+1))/pow((commonPow-1),2);
}

int isPositive(double num) {
	return num > 0;
}

int isNegative(double num) {
	return num < 0;
}

double decreaseA() {
	a += -(INTERVAL/2.0) + PUSH;
	return a;
}

double increaseB() {
	b += INTERVAL/2.0 + PUSH;
	return b;
}

double findViaBisection(double lVal, double rVal) {
	double midpoint, funcMid;
	a = lVal;
	b = rVal;

	midpoint = (a+b)/2.0;
	funcMid = func(midpoint);

	while(fabs(funcMid) > ROOTTOLERANCE) {


		funcA = func(a);
		funcB = func(b);


		if(isNegative(funcA*funcB)) {
			//root found between a and b

			// root is between a and midPoint
			if(isNegative(funcMid*funcA))
				b = midpoint;
			// root is between midPoint and b
			else
				a = midpoint;
		}else {
			// no root found between a and b
			// enlarging the interval between a and b
			decreaseA();
			increaseB();
		}

		// calculating the new midPoint and f(midPoint)
		midpoint = (a+b)/2.0;
		funcMid = func(midpoint);
	}

	return midpoint;
}

double findViaSecant(double lVal, double rVal) {
	double tempRoot, funcTempRoot;
	a = lVal;
	b = rVal;

	tempRoot = (a+b)/2.0;
	funcTempRoot = func(tempRoot);

	while(fabs(funcTempRoot) > ROOTTOLERANCE) {


		funcA = func(a);
		funcB = func(b);

		tempRoot = a + (b - a) * (-funcA) / (funcB - funcA);
		a = b;
		b = tempRoot;

		// calculating the new midPoint and f(midPoint)
		funcTempRoot = func(tempRoot);
	}

	return tempRoot;
}

double findViaFalsePosition(double lVal, double rVal) {
	double tempRoot, funcTempRoot;
	a = lVal;
	b = rVal;

	tempRoot = (a+b)/2.0;
	funcTempRoot = func(tempRoot);

	while(fabs(funcTempRoot) > ROOTTOLERANCE) {
		funcA = func(a);
		funcB = func(b);

		tempRoot = a + (b - a) * (-funcA) / (funcB - funcA);

		if (isPositive(funcTempRoot*funcA))
			a = tempRoot;
		else
			b = tempRoot;

		funcTempRoot = func(tempRoot);
	}

	return tempRoot;
}

double findViaNewtonRaphson(double val) {
	double tempRoot, funcTempRoot;

	tempRoot = val;
	funcTempRoot = func(tempRoot);

	while(fabs(funcTempRoot) > ROOTTOLERANCE) {
		tempRoot -= funcTempRoot/defFunc(tempRoot);
		funcTempRoot = func(tempRoot);
	}

	return tempRoot;
}
